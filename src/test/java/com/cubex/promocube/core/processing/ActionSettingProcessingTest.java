package com.cubex.promocube.core.processing;

import com.cubex.promocube.PromocubeApplication;
import com.cubex.promocube.core.database.entity.action.Action;
import com.cubex.promocube.core.database.entity.action.ActionMechanicsType;
import com.cubex.promocube.core.database.entity.actionsettings.*;
import com.cubex.promocube.core.database.entity.pricing.PriceList;
import com.cubex.promocube.core.database.entity.pricing.PriceListValue;
import com.cubex.promocube.core.database.entity.product.Product;
import com.cubex.promocube.core.domain.service.PriceListValueService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.*;

@SpringBootTest(classes = {PromocubeApplication.class})
@ActiveProfiles("test")
class ActionSettingProcessingTest {
    @Autowired
    private ActionSettingProcessing processing;

    @MockBean
    private PriceListValueService priceListValueService;

    ActionSettings makeActionSettings(Long id, Action action, PriceList priceList){
        Calendar begin = new GregorianCalendar(2021,Calendar.MAY,1,3,0,0);
        Calendar end = new GregorianCalendar(2030,Calendar.DECEMBER,1,3,0,0);

        ActionSettings actionSettings = new ActionSettings();
        actionSettings.setId(id);
        actionSettings.setAction(action);
        actionSettings.setMechanicsType(action.getMechanicsType());
        actionSettings.setDateBegin(begin.getTime());
        actionSettings.setDateEnd(end.getTime());
        actionSettings.setPriceList(priceList);

        actionSettings.setUserRoles(new ArrayList<>());
        actionSettings.setIsCouponRequired(false);
        actionSettings.setDiscountFactor(BigDecimal.valueOf(0));
        actionSettings.setDiscountPayoutType(DiscountPayoutType.MANY);

        return actionSettings;
    }

    ActionSettingsCondition makeActionSettingsCondition(Long id, OperationType operationType, BigDecimal discountValue){
        ActionSettingsCondition condition = new ActionSettingsCondition();
        condition.setId(id);
        condition.setOperationType(operationType);
        condition.setDiscountValue(discountValue);
        return condition;
    }

    ActionSettingsProduct makeActionSettingsProduct(Long id, Product product, ActionSettingsCondition actionSettingsCondition){
        ActionSettingsProduct actionSettingsProduct = new ActionSettingsProduct();
        actionSettingsProduct.setId(id);
        actionSettingsProduct.setProduct(product);
        actionSettingsProduct.setCondition(actionSettingsCondition);
        return actionSettingsProduct;
    }

    PriceListValue makePriceListValue (Long id, PriceList priceList, Product product, Date currentDate, BigDecimal price) {
        PriceListValue priceListValue = new PriceListValue();
        priceListValue.setId(id);
        priceListValue.setProduct(product);
        priceListValue.setPriceList(priceList);
        priceListValue.setDiscountValue(price);
        priceListValue.setDateBegin(currentDate);
        return priceListValue;
    }

    public void CheckExtraBonusActionApplaying(Action action, PriceList priceList, Product product, DiscountPayoutType discountPayoutType, BigDecimal currentPrice, OperationType operationType){
        Date currentDate = new Date();

        Product productAdd = new Product(500L);
        ActionSettings actionSettings = makeActionSettings(100L, action, priceList);
        actionSettings.setDiscountPayoutType(discountPayoutType);
        ActionSettingsCondition actionSettingsCondition = makeActionSettingsCondition(100L, operationType, BigDecimal.valueOf(0));
        ActionSettingsProduct actionSettingsProduct = makeActionSettingsProduct(100L, product, actionSettingsCondition);
        ActionSettingsProduct actionSettingsProductAdd = makeActionSettingsProduct(100L, productAdd, actionSettingsCondition);

        actionSettings.setConditions(Collections.singletonList(actionSettingsCondition));
        actionSettingsCondition.setProducts(Arrays.asList(actionSettingsProduct,actionSettingsProductAdd));

        PriceListValue priceListValue = makePriceListValue(100L, priceList, product, currentDate, currentPrice);
        PriceListValue priceListValueAdd = makePriceListValue(100L, priceList, productAdd, currentDate, new BigDecimal(105));

        List<Long> productsId = Arrays.asList(product.getId(), productAdd.getId());
        when(priceListValueService.getPriceListValues(currentDate,productsId)).thenReturn(Arrays.asList(priceListValue,priceListValueAdd));

        assertEquals(actionSettings.getConditions().size(),1);

        assertTrue(actionSettings.getConditions().stream()
                .allMatch(x->x.getOperationType().equals(operationType) &&
                        x.getDiscountValue().equals(BigDecimal.ZERO)));

        List<Product> products = Arrays.asList(product, productAdd);
        processing.execute(actionSettings, products, currentDate);

        assertEquals(actionSettings.getConditions().size(),2);

        assertTrue(actionSettings.getConditions().stream()
                .filter(x->x.getProducts().contains(product))
                .allMatch(x-> x.getDiscountValue().equals(priceListValue.getDiscountValue()) &&
                        x.getOperationType().equals(operationType)));

        assertTrue(actionSettings.getConditions().stream()
                .filter(x->x.getProducts().contains(productAdd))
                .allMatch(x-> x.getDiscountValue().equals(priceListValueAdd.getDiscountValue()) &&
                        x.getOperationType().equals(operationType)));

    }

    @Test
    public void executeL016_CheckPriceListApplaying(){
        Action action = new Action(100L);
        action.setName("Ночь скидок");
        action.setMechanicsType(ActionMechanicsType.DISCOUNT_BY_PRICE_LIST);

        Product product = new Product(100L);
        PriceList priceList = new PriceList(100L);
        BigDecimal currentPrice = new BigDecimal(100000);

        CheckExtraBonusActionApplaying(action, priceList, product, DiscountPayoutType.MANY, currentPrice, OperationType.PRICE);
    }

    @Test
    public void executeL005_CheckExtraBonusActionApplaying(){
        Action action = new Action(100L);
        action.setName("CashBack bonuses");
        action.setMechanicsType(ActionMechanicsType.DISCOUNT_BY_PRICE_LIST);

        Product product = new Product(100L);
        PriceList priceList = new PriceList(100L);
        BigDecimal currentPrice = new BigDecimal(300000);

        CheckExtraBonusActionApplaying(action, priceList, product, DiscountPayoutType.BONUSES, currentPrice, OperationType.SUMMA);
    }
}