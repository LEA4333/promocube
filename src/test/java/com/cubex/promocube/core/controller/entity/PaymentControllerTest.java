package com.cubex.promocube.core.controller.entity;

import com.cubex.promocube.PromocubeApplication;
import com.cubex.promocube.core.domain.facade.PaymentFacade;
import com.cubex.promocube.core.dto.action.ActionDto;
import com.cubex.promocube.core.dto.coupon.CouponSettingsDto;
import com.cubex.promocube.core.dto.payment.PaymentDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = {PromocubeApplication.class})
@ActiveProfiles("test")
class PaymentControllerTest {
    @Autowired
    private PaymentController controller;

    @MockBean
    private PaymentFacade facade;

    private final String path = "/api/payment";
    private MockMvc mockMvc;

    @BeforeEach
    public void init(){
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    void create() throws Exception {
        MockHttpServletRequestBuilder builder = post(path)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"id\": 1,\n" +
                        "  \"name\": \"Kaspi QR Gold\",\n" +
                        "  \"paymentType\": 1\n" +
                        "}");

        doNothing().when(facade).create(any(PaymentDto.class));

        mockMvc.perform(builder)
                .andExpect(status().isCreated())
                .andExpect(content().string(""));

        verify(facade, Mockito.times(1)).create(any(PaymentDto.class));
    }

    @Test
    void getById() throws Exception {
        PaymentDto dto = new PaymentDto(100L);
        when(facade.findById(dto.getId())).thenReturn(Optional.of(dto));

        MockHttpServletRequestBuilder builder = get(path+"/100");

        mockMvc.perform(builder)
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        verify(facade, Mockito.times(1)).findById(100L);
    }

    @Test
    void remove()throws Exception {
        doNothing().when(facade).delete(any(Long.class));

        MockHttpServletRequestBuilder builder = delete(path+"/100");
        mockMvc.perform(builder)
                .andExpect(status().isAccepted())
                .andExpect(content().string(""));

        verify(facade, Mockito.times(1)).delete(100L);
    }
}