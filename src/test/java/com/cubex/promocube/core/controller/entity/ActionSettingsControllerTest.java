package com.cubex.promocube.core.controller.entity;

import com.cubex.promocube.PromocubeApplication;
import com.cubex.promocube.core.domain.facade.ActionSettingsFacade;
import com.cubex.promocube.core.dto.actionsettigs.ActionSettingsDto;
import com.cubex.promocube.core.dto.intersection.ActionIntersectionDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = {PromocubeApplication.class})
@ActiveProfiles("test")
class ActionSettingsControllerTest {
    @Autowired
    private ActionSettingsController controller;

    @MockBean
    private ActionSettingsFacade facade;

    private final String path = "/api/actionSettings";
    private MockMvc mockMvc;

    @BeforeEach
    public void init(){
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    void create() throws Exception {
        MockHttpServletRequestBuilder builder = post(path)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"actionId\": 1,\n" +
                        "  \"conditions\": [\n" +
                        "    {\n" +
                        "      \"discountValue\": 10,\n" +
                        "      \"operationType\": \"SUMMA\",\n" +
                        "      \"products\": [\n" +
                        "        {\n" +
                        "          \"productId\": 1\n" +
                        "        }\n" +
                        "      ]\n" +
                        "    }\n" +
                        "  ],\n" +
                        "  \"dateBegin\": \"2021-01-01\",\n" +
                        "  \"dateEnd\": \"2021-12-30\",\n" +
                        "  \"discountPayoutType\": \"MANY\",\n" +
                        "  \"id\": 1\n" +
                        "}");

        doNothing().when(facade).create(any(ActionSettingsDto.class));

        mockMvc.perform(builder)
                .andExpect(status().isCreated())
                .andExpect(content().string(""));

        verify(facade, Mockito.times(1)).create(any(ActionSettingsDto.class));
    }

    @Test
    void createWholeData() throws Exception {
        MockHttpServletRequestBuilder builder = post(path)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"actionId\": 1,\n" +
                        "  \"conditions\": [\n" +
                        "    {\n" +
                        "      \"discountValue\": 10,\n" +
                        "      \"operationType\": \"SUMMA\",\n" +
                        "      \"products\": [\n" +
                        "        {\n" +
                        "          \"productId\": 1\n" +
                        "        }\n" +
                        "      ]\n" +
                        "    }\n" +
                        "  ],\n" +
                        "  \"dateBegin\": \"2021-01-01\",\n" +
                        "  \"dateEnd\": \"2021-12-30\",\n" +
                        "  \"discountPayoutType\": \"MANY\",\n" +
                        "  \"id\": 1,\n" +
                        "  \"payments\": [\n" +
                        "    {\n" +
                        "      \"paymentId\": 1\n" +
                        "    }\n" +
                        "  ],\n" +
                        "  \"userRoles\": [\n" +
                        "    {\n" +
                        "      \"userRoleId\": 1\n" +
                        "    }\n" +
                        "  ]\n" +
                        "}");

        mockMvc.perform(builder)
                .andExpect(status().isCreated())
                .andExpect(content().string(""));

        verify(facade, Mockito.times(1)).create(any(ActionSettingsDto.class));
    }

    @Test
    void remove() throws Exception {
        doNothing().when(facade).delete(any(Long.class));

        MockHttpServletRequestBuilder builder = delete(path+"/100");
        mockMvc.perform(builder)
                .andExpect(status().isAccepted())
                .andExpect(content().string(""));

        verify(facade, Mockito.times(1)).delete(100L);
    }

    @Test
    void selectProcessed() throws Exception {
        Calendar calendar = new GregorianCalendar(2021,Calendar.MAY,2,3,0,0);
        Date period = calendar.getTime();

        List<ActionSettingsDto> actionSettingsDtoList = new ArrayList<>();
        actionSettingsDtoList.add(new ActionSettingsDto(100L));
        actionSettingsDtoList.add(new ActionSettingsDto(200L));
        actionSettingsDtoList.add(new ActionSettingsDto(300L));

        when(facade.findAllProcessed(period)).thenReturn(actionSettingsDtoList);

        MockHttpServletRequestBuilder builder = post(path+"/current")
        .contentType(MediaType.APPLICATION_JSON)
        .content("{\n" +
                "  \"date\": \"2021-05-02\"\n" +
                "}");

        mockMvc.perform(builder)
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        verify(facade, Mockito.times(1)).findAllProcessed(period);
    }
}