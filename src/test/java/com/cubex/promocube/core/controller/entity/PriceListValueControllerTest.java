package com.cubex.promocube.core.controller.entity;

import com.cubex.promocube.PromocubeApplication;
import com.cubex.promocube.core.controller.entity.PriceListValueController;
import com.cubex.promocube.core.domain.facade.PriceListValueFacade;
import com.cubex.promocube.core.dto.pricing.PriceListDto;
import com.cubex.promocube.core.dto.pricing.PriceListValueDto;
import com.cubex.promocube.core.dto.pricing.PriceListValuesRequestDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest(classes = {PromocubeApplication.class})
@ActiveProfiles("test")
class PriceListValueControllerTest {
    @Autowired
    PriceListValueController controller;

    @MockBean
    private PriceListValueFacade facade;

    private MockMvc mockMvc;
    private final String path="/api/priceListValue";

    @BeforeEach
    public void init(){
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    void create() throws Exception {
        MockHttpServletRequestBuilder builder = post(path)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"dateBegin\": \"2021-01-01\",\n" +
                        "  \"discountValue\": 10,\n" +
                        "  \"id\": 100,\n" +
                        "  \"priceListId\": 1,\n" +
                        "  \"productId\": 1\n" +
                        "}");

        doNothing().when(facade).create(any(PriceListValueDto.class));

        mockMvc.perform(builder)
                .andExpect(status().isCreated());

        verify(facade,times(1)).create(any(PriceListValueDto.class));
    }

    @Test
    void getById() throws Exception {
        PriceListValueDto dto = new PriceListValueDto(100L);
        when(facade.findById(100L)).thenReturn(Optional.of(dto));

        MockHttpServletRequestBuilder builder = get(path+"/100")
                .contentType(MediaType.APPLICATION_JSON);

        mockMvc.perform(builder)
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    void remove()throws Exception {
        doNothing().when(facade).delete(any(Long.class));

        MockHttpServletRequestBuilder builder = delete(path+"/100");

        mockMvc.perform(builder)
                .andExpect(status().isAccepted())
                .andExpect(content().string(""));

        verify(facade, Mockito.times(1)).delete(100L);
    }

    @Test
    void getPriceListValues() throws Exception {
        List<PriceListValueDto> list = new ArrayList<>();
        list.add(new PriceListValueDto(100L));
        list.add(new PriceListValueDto(200L));
        list.add(new PriceListValueDto(300L));

        when(facade.getPriceListValues(any(PriceListValuesRequestDto.class))).thenReturn(list);

        MockHttpServletRequestBuilder builder = post(path+"/current")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"period\": \"2021-07-01\",\n" +
                        "  \"productsId\": [\n" +
                        "    0\n" +
                        "  ]\n" +
                        "}");

        mockMvc.perform(builder)
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        verify(facade, Mockito.times(1)).getPriceListValues(any(PriceListValuesRequestDto.class));
    }
}