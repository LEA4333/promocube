package com.cubex.promocube.core.controller.entity;

import com.cubex.promocube.PromocubeApplication;
import com.cubex.promocube.core.domain.facade.ProductUnionFacade;
import com.cubex.promocube.core.dto.productUnion.ProductUnionDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = {PromocubeApplication.class})
@ActiveProfiles("test")
class ProductUnionTest {
    @Autowired
    private ProductUnionController controller;

    @MockBean
    private ProductUnionFacade facade;

    private final String path = "/api/productUnion";
    private MockMvc mockMvc;

    @BeforeEach
    public void init(){
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    void create() throws Exception {
        MockHttpServletRequestBuilder builder = post(path)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"id\": 1,\n" +
                        "  \"name\": \"test\"\n" +
                        "}");

        doNothing().when(facade).create(any(ProductUnionDto.class));

        mockMvc.perform(builder)
                .andExpect(status().isCreated())
                .andExpect(content().string(""));

        verify(facade, Mockito.times(1)).create(any(ProductUnionDto.class));
    }

    @Test
    void getById() throws Exception {
        ProductUnionDto dto = new ProductUnionDto(100L);
        when(facade.findById(100L)).thenReturn(Optional.of(dto));

        MockHttpServletRequestBuilder builder = get(path+"/100");

        mockMvc.perform(builder)
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        verify(facade, Mockito.times(1)).findById(100L);
    }

    @Test
    void remove()throws Exception {
        doNothing().when(facade).delete(any(Long.class));

        MockHttpServletRequestBuilder builder = delete(path+"/100");
        mockMvc.perform(builder)
                .andExpect(status().isAccepted())
                .andExpect(content().string(""));

        verify(facade, Mockito.times(1)).delete(100L);
    }
}