package com.cubex.promocube.core.database.entity.purchase;

import com.cubex.promocube.core.database.entity.action.Action;
import com.cubex.promocube.core.database.entity.actionsettings.ActionSettings;
import com.cubex.promocube.core.database.entity.actionsettings.DiscountPayoutType;
import com.cubex.promocube.core.database.entity.product.Product;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Setter
@ToString
@NoArgsConstructor

@Entity
public class PurchaseDiscount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    private Action action;

    @ManyToOne(fetch = FetchType.LAZY)
    ActionSettings actionSettings;

    @ManyToOne(fetch = FetchType.LAZY)
    PurchaseProduct purchaseProduct;

    @Column
    private BigDecimal sum;

    @Enumerated(EnumType.ORDINAL)
    private DiscountPayoutType discountPayoutType;

    public PurchaseDiscount(ActionSettings actionSettings, PurchaseProduct purchaseProduct, DiscountPayoutType discountPayoutType, BigDecimal sum){
        this.action = actionSettings.getAction();
        this.actionSettings = actionSettings;
        this.purchaseProduct = purchaseProduct;
        this.discountPayoutType = discountPayoutType;
        this.sum = sum;
    }
}
