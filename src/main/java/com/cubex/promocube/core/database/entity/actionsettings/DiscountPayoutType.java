package com.cubex.promocube.core.database.entity.actionsettings;

public enum DiscountPayoutType {
    MANY,
    BONUSES
}
