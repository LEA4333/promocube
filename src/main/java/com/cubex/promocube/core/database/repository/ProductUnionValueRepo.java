package com.cubex.promocube.core.database.repository;

import com.cubex.promocube.core.database.entity.productUnion.ProductUnionValue;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Date;
import java.util.List;

public interface ProductUnionValueRepo extends CrudRepository<ProductUnionValue, Long>, PagingAndSortingRepository<ProductUnionValue, Long> {
    @Query(nativeQuery = true, value = "SELECT * FROM product_union_value u " +
            "INNER JOIN (SELECT u.product_id, u.product_union_id, MAX(u.date_begin) as date_begin, MAX(u.id) as max_id FROM product_union_value u WHERE u.date_begin <= :period and u.product_id IN (:productsId) GROUP BY u.product_id, u.product_union_id) as tb " +
            "ON u.date_begin = tb.date_begin AND u.id = tb.max_id AND u.product_id = tb.product_id AND u.product_union_id = tb.product_union_id " +
            "WHERE u.status='ACTIVATED'")
    List<ProductUnionValue> getValues(Date period, List<Long> productsId);

    @Query(nativeQuery = true, value = "SELECT * FROM product_union_value u " +
            "INNER JOIN (SELECT u.product_id, u.product_union_id, MAX(u.date_begin) as date_begin, MAX(u.id) as max_id FROM product_union_value u WHERE u.date_begin <= :period GROUP BY u.product_id, u.product_union_id) as tb " +
            "ON u.date_begin = tb.date_begin AND u.id = tb.max_id AND u.product_id = tb.product_id AND u.product_union_id = tb.product_union_id " +
            "WHERE u.status='ACTIVATED'")
    List<ProductUnionValue> getValues(Date period);
}
