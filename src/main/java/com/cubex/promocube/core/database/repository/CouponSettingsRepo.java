package com.cubex.promocube.core.database.repository;

import com.cubex.promocube.core.database.entity.coupon.CouponSettings;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface CouponSettingsRepo extends CrudRepository<CouponSettings, Long>, PagingAndSortingRepository<CouponSettings, Long> {
    Optional<CouponSettings> findById(Long id);
}
