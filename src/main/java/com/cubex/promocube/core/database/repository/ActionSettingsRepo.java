package com.cubex.promocube.core.database.repository;

import com.cubex.promocube.core.database.entity.actionsettings.ActionSettings;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface ActionSettingsRepo  extends CrudRepository<ActionSettings, Long>, PagingAndSortingRepository<ActionSettings, Long> {
    Optional<ActionSettings> findById(Long id);

    @Query(nativeQuery = true,value =
    "SELECT * FROM action_settings u WHERE u.date_begin <= :date AND u.date_end >= :date")
    List<ActionSettings> findAllProcessed(@Param("date") Date data);
}
