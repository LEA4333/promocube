package com.cubex.promocube.core.database.entity.actionsettings;

import com.cubex.promocube.core.database.entity.product.Product;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor

@Entity
public class ActionSettingsProduct {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @ManyToOne(fetch = FetchType.LAZY)
    private ActionSettingsCondition condition;

    public ActionSettingsProduct(Long id, Product product, ActionSettingsCondition condition) {
        this.id = id;
        this.product = product;
        this.condition = condition;
    }

    public ActionSettingsProduct(Product product, ActionSettingsCondition condition) {
        this.product = product;
        this.condition = condition;
    }
}
