package com.cubex.promocube.core.database.entity.action;

public enum ActionMechanicsType {
    NONE,
    DISCOUNT_PERCENT_SUMMA,
    DISCOUNT_BY_PRICE_LIST,
    DISCOUNT_FOR_PRODUCTS_SET,
    DISCOUNT_FOR_PRODUCTS_CASCADE,
    BONUS_PERCENT_SUMMA,
    PRICE_FOR_SERVICES,
    LOYALTY_PROGRAM
}



