package com.cubex.promocube.core.database.entity.actionsettings;

import com.cubex.promocube.core.database.entity.product.ProductUnion;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor

@Entity
public class ActionSettingsProductUnion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    private ProductUnion productUnion;

    @ManyToOne(fetch = FetchType.LAZY)
    private ActionSettingsCondition condition;

    public ActionSettingsProductUnion(Long id, ProductUnion productUnion, ActionSettingsCondition condition) {
        this.id = id;
        this.productUnion = productUnion;
        this.condition = condition;
    }
}
