package com.cubex.promocube.core.database.entity.productUnion;

import com.cubex.promocube.core.database.entity.product.Product;
import com.cubex.promocube.core.database.entity.product.ProductUnion;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor

@Entity
public class ProductUnionValue {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Date dateBegin;

    @ManyToOne
    private ProductUnion productUnion;

    @ManyToOne
    private Product product;

    @Enumerated(EnumType.STRING)
    private ProductUnionValueStatus status;
}
