package com.cubex.promocube.core.database.repository;

import com.cubex.promocube.core.database.entity.product.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface ProductRepo  extends CrudRepository<Product, Long>, PagingAndSortingRepository<Product, Long> {
    Optional<Product> findById(Long id);

    List<Product> findTop100By();
}
