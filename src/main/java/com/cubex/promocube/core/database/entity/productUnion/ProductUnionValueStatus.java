package com.cubex.promocube.core.database.entity.productUnion;

public enum ProductUnionValueStatus {
    NONE,
    ACTIVATED,
    DEACTIVATED
}
