package com.cubex.promocube.core.database.repository;

import com.cubex.promocube.core.database.entity.purchase.Purchase;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface PurchaseRepo extends CrudRepository<Purchase, Long>, PagingAndSortingRepository<Purchase, Long> {
    Optional<Purchase> findById(Long id);
}
