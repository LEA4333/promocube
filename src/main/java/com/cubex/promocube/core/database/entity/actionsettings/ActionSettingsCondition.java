package com.cubex.promocube.core.database.entity.actionsettings;

import com.cubex.promocube.core.database.entity.product.Product;
import com.cubex.promocube.core.database.entity.product.ProductGroup;
import com.cubex.promocube.core.database.entity.product.ProductUnion;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor

@Entity
public class ActionSettingsCondition {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private OperationType operationType;

    @Column
    private BigDecimal discountValue;

    @ToString.Exclude
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    List<ActionSettingsProduct> products = new ArrayList<>();

    @ToString.Exclude
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    List<ActionSettingsProductGroup> productGroups = new ArrayList<>();

    @ToString.Exclude
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    List<ActionSettingsProductUnion> productUnions = new ArrayList<>();

    public ActionSettingsCondition(Long id, OperationType operationType, BigDecimal discountValue) {
        this.id = id;
        this.operationType = operationType;
        this.discountValue = discountValue;
    }

    public boolean isProductContains(Product product){
        return this.getProducts().stream().anyMatch(x -> x.getProduct().equals(product));
    }

    public boolean isProductGroupContains(ProductGroup productGroup){
        return this.getProductGroups().stream().anyMatch(x -> x.getProductGroup().equals(productGroup));
    }

    public boolean isProductUnionContains(ProductUnion productUnion){
        return this.getProductUnions().stream().anyMatch(x -> x.getProductUnion().equals(productUnion));
    }
}
