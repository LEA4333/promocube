package com.cubex.promocube.core.database.entity.coupon;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor

@Entity
public class Coupon {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Enumerated(EnumType.ORDINAL)
    private CouponStatus couponStatus;

    @ManyToOne(fetch = FetchType.EAGER)
    private CouponSettings couponSettings;

    @Column
    private BigDecimal sum;

    @PrePersist
    private void prePersist(){
        if(name==null){
            name = UUID.randomUUID().toString().replace("-","F").toUpperCase();
        }
    }

    public Coupon(Long id, String name, CouponStatus couponStatus, CouponSettings couponSettings) {
        this.id = id;
        this.name = name;
        this.couponStatus = couponStatus;
        this.couponSettings = couponSettings;
    }
}
