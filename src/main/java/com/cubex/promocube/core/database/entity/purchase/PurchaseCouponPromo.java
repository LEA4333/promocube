package com.cubex.promocube.core.database.entity.purchase;

import com.cubex.promocube.core.database.entity.action.Action;
import com.cubex.promocube.core.database.entity.actionsettings.ActionSettings;
import com.cubex.promocube.core.database.entity.coupon.Coupon;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
@NoArgsConstructor

@Entity
public class PurchaseCouponPromo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    private Coupon coupon;

    @ManyToOne(fetch = FetchType.LAZY)
    private Action action;

    @ManyToOne(fetch = FetchType.LAZY)
    ActionSettings actionSettings;

    public PurchaseCouponPromo(Long id, Coupon coupon, Action action, ActionSettings actionSettings) {
        this.id = id;
        this.coupon = coupon;
        this.action = action;
        this.actionSettings = actionSettings;
    }
}
