package com.cubex.promocube.core.database.entity.actionsettings;

import com.cubex.promocube.core.database.entity.product.ProductGroup;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor

@Entity
public class ActionSettingsProductGroup {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    private ProductGroup productGroup;

    @ManyToOne(fetch = FetchType.LAZY)
    private ActionSettingsCondition condition;

    public ActionSettingsProductGroup(Long id, ProductGroup productGroup, ActionSettingsCondition condition) {
        this.id = id;
        this.productGroup = productGroup;
        this.condition = condition;
    }
}
