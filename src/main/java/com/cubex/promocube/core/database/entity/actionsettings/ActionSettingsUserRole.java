package com.cubex.promocube.core.database.entity.actionsettings;

import com.cubex.promocube.core.database.entity.user.UserRole;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter

@Entity
public class ActionSettingsUserRole {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private UserRole userRole;

}
