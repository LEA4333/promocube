package com.cubex.promocube.core.database.repository;

import com.cubex.promocube.core.database.entity.department.Department;
import com.cubex.promocube.core.database.entity.payment.Payment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface DepartmentRepo extends CrudRepository<Department, Long>, PagingAndSortingRepository<Department, Long> {
    Optional<Department> findById(Long id);
}
