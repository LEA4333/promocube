package com.cubex.promocube.core.database.entity.action;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor

@Entity
public class Action {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Enumerated(EnumType.ORDINAL)
    private ActionMechanicsType mechanicsType;

    public Action(Long id) {
        this.id = id;
    }

    public Action(Long id, ActionMechanicsType mechanicsType) {
        this.id = id;
        this.mechanicsType = mechanicsType;
    }

    public Action(Long id, String name, ActionMechanicsType mechanicsType) {
        this.id = id;
        this.name = name;
        this.mechanicsType = mechanicsType;
    }
}
