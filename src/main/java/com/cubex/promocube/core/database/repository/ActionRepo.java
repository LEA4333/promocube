package com.cubex.promocube.core.database.repository;

import com.cubex.promocube.core.database.entity.action.Action;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface ActionRepo extends CrudRepository<Action, Long>, PagingAndSortingRepository<Action, Long> {
    Optional<Action> findById(Long id);
}
