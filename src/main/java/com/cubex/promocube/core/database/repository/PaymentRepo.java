package com.cubex.promocube.core.database.repository;

import com.cubex.promocube.core.database.entity.payment.Payment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface PaymentRepo extends CrudRepository<Payment, Long>, PagingAndSortingRepository<Payment, Long> {
    Optional<Payment> findById(Long id);
}
