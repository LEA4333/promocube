package com.cubex.promocube.core.database.entity.actionsettings;

import com.cubex.promocube.core.database.entity.action.Action;
import com.cubex.promocube.core.database.entity.action.ActionMechanicsType;
import com.cubex.promocube.core.database.entity.pricing.PriceList;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor

@Entity
public class ActionSettings {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    private Action action;

    @Enumerated(EnumType.ORDINAL)
    private ActionMechanicsType mechanicsType;

    @Enumerated(EnumType.ORDINAL)
    private DiscountPayoutType discountPayoutType;

    @Column
    private Date dateBegin;

    @Column
    private Date dateEnd;

    @Column
    private Boolean isCouponRequired;

    @ManyToOne
    private PriceList priceList;

    @Column
    private BigDecimal discountFactor;

    @ToString.Exclude
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    List<ActionSettingsCondition> conditions;

    @ToString.Exclude
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    List<ActionSettingsUserRole> userRoles = new ArrayList<>();

    @ToString.Exclude
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    List<ActionSettingsPayment> payments =new ArrayList<>();

    @ToString.Exclude
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    List<ActionSettingsCouponPromo> couponPromos =new ArrayList<>();

    public ActionSettings(Long id) {
        this.id = id;
    }
}
