package com.cubex.promocube.core.database.repository;

import com.cubex.promocube.core.database.entity.product.ProductUnion;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductUnionRepo extends CrudRepository<ProductUnion, Long>, PagingAndSortingRepository<ProductUnion, Long> {

}
