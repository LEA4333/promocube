package com.cubex.promocube.core.database.entity.purchase;

import com.cubex.promocube.core.database.entity.action.Action;
import com.cubex.promocube.core.database.entity.actionsettings.ActionSettings;
import com.cubex.promocube.core.database.entity.coupon.Coupon;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
@NoArgsConstructor

@Entity
public class PurchaseCoupon {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Coupon coupon;

    @ManyToOne(fetch = FetchType.LAZY)
    private Action action;

    @ManyToOne(fetch = FetchType.LAZY)
    ActionSettings actionSettings;

    @Column(nullable = true)
    private Boolean applying;

    public PurchaseCoupon(Long id, Coupon coupon) {
        this.id = id;
        this.coupon = coupon;
    }
}
