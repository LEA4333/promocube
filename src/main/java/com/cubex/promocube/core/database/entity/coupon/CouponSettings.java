package com.cubex.promocube.core.database.entity.coupon;

import com.cubex.promocube.core.database.entity.action.Action;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor

@Entity
public class CouponSettings {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    private Action action;

    @Column
    private Date dateBegin;

    @Column
    private Date dateEnd;

    public CouponSettings(Long id, String name, Action action, Date dateBegin, Date dateEnd) {
        this.id = id;
        this.name = name;
        this.action = action;
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
    }
}
