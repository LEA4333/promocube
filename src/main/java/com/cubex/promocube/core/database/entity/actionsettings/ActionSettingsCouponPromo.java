package com.cubex.promocube.core.database.entity.actionsettings;

import com.cubex.promocube.core.database.entity.coupon.CouponSettings;
import com.cubex.promocube.core.database.entity.payment.Payment;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor

@Entity
public class ActionSettingsCouponPromo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private CouponSettings couponSettings;

    @Column
    private BigDecimal sum;

    public ActionSettingsCouponPromo(Long id, CouponSettings couponSettings) {
        this.id = id;
        this.couponSettings = couponSettings;
    }

    public ActionSettingsCouponPromo(Long id, CouponSettings couponSettings, BigDecimal sum) {
        this.id = id;
        this.couponSettings = couponSettings;
        this.sum = sum;
    }
}
