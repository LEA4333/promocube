package com.cubex.promocube.core.dto.productUnion;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor

@ApiModel
public class ProductUnionDto {
    @ApiModelProperty(example = "1", required = true)
    @NotNull
    private Long id;

    @ApiModelProperty(example = "test", required = true)
    @NotNull
    private String name;

    public ProductUnionDto(long id) {
        this.id = id;
    }
}
