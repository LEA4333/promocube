package com.cubex.promocube.core.dto.productUnion;

import com.cubex.promocube.core.database.entity.productUnion.ProductUnionValueStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor

@ApiModel
public class ProductUnionValueDto {
    @NotNull
    @ApiModelProperty(example = "2021-01-01", required = true, position = 1)
    private Date dateBegin;

    @NotNull
    @ApiModelProperty(example = "1", required = true, position = 1)
    private Long productUnionId;

    @NotNull
    @ApiModelProperty(example = "1", required = true, position = 1)
    private Long productId;

    @NotNull
    @ApiModelProperty(example = "ACTIVATED", required = true, position = 1)
    private ProductUnionValueStatus status;
}
