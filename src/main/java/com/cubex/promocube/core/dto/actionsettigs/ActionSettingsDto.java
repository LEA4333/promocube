package com.cubex.promocube.core.dto.actionsettigs;

import com.cubex.promocube.core.database.entity.actionsettings.DiscountPayoutType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Getter
@Setter

@ApiModel
@NoArgsConstructor
public class ActionSettingsDto {
    @NotNull
    @ApiModelProperty(example = "1", required = true)
    private Long id;

    @NotNull
    @ApiModelProperty(example = "1", required = true)
    private Long actionId;

    @NotNull
    @ApiModelProperty(example = "MANY", required = true)
    private DiscountPayoutType discountPayoutType;

    @NotNull
    @ApiModelProperty(example = "2021-01-01", required = true)
    private Date dateBegin;

    @NotNull
    @ApiModelProperty(example = "2021-12-30", required = true)
    private Date dateEnd;

    @ApiModelProperty(hidden = true)
    private Long priceListId;

    @ApiModelProperty(hidden = true, example = "0")
    private BigDecimal discountFactor;

    @ApiModelProperty(hidden = true)
    private Boolean isCouponRequired;

    List<ActionSettingsConditionDto> conditions;

    List<ActionSettingsUserRoleDto> userRoles;

    List<ActionSettingsPaymentDto> payments;

    List<ActionSettingsCouponPromoDto> couponPromos;

    public ActionSettingsDto(Long id) {
        this.id=id;
    }
}
