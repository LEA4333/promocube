package com.cubex.promocube.core.dto.actionsettigs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Setter

@ApiModel
public class ActionSettingsCouponPromoDto {
    @NotNull
    @ApiModelProperty(example = "1", required = true)
    private Long couponSettingsId;

    @NotNull
    @ApiModelProperty(example = "0", required = true)
    private BigDecimal sum;
}
