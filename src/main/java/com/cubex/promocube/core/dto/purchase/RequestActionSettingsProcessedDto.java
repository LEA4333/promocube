package com.cubex.promocube.core.dto.purchase;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
@ToString

@ApiModel()
public class RequestActionSettingsProcessedDto {
    @ApiModelProperty(example = "2021-05-02T12:30:00", required = true)
    @NotNull
    private Date date;
}
