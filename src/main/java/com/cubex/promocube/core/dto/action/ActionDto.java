package com.cubex.promocube.core.dto.action;

import com.cubex.promocube.core.database.entity.action.ActionMechanicsType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor

@ApiModel
public class ActionDto {
    @ApiModelProperty(example = "1", required = true)
    @NotNull
    private Long id;

    @ApiModelProperty(example = "promo", required = true)
    @NotBlank
    private String name;

    @ApiModelProperty(example = "1", required = true)
    @NotNull
    private ActionMechanicsType mechanicsType;

    public ActionDto(Long id) {
        this.id = id;
    }
}
