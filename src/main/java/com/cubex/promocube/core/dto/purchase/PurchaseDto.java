package com.cubex.promocube.core.dto.purchase;

import com.cubex.promocube.core.database.entity.payment.Payment;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor

@ApiModel
public class PurchaseDto {
    @ApiModelProperty(example = "1", required = true)
    @NotNull
    private Long id;

    @ApiModelProperty(example = "2021-05-01", required = true)
    @NotNull
    private Date date;

    @NotNull
    @ApiModelProperty(example = "1", required = true)
    private Long userRoleId;

    @NotNull
    @ApiModelProperty(example = "1", required = true)
    private Long paymentId;

    @NotNull
    @ApiModelProperty(example = "1", required = true)
    private Long departmentId;

    List<PurchaseProductDto> products;

    @ApiModelProperty(hidden = true)
    List<PurchaseCouponDto> coupons = new ArrayList<>();

    @ApiModelProperty(hidden = true)
    List<PurchaseDiscountDto> discounts = new ArrayList<>();

    @ApiModelProperty(hidden = true)
    List<PurchaseCouponPromoDto> couponPromos = new ArrayList<>();

    public PurchaseDto(long id) {
        this.id = id;
    }
}
