package com.cubex.promocube.core.dto.pricing;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter

@ApiModel
@NoArgsConstructor
public class PriceListDto {
    @ApiModelProperty(example = "1", required = true)
    @NotNull
    private Long id;

    @ApiModelProperty(example = "Main", required = true)
    @NotNull
    private String name;

    public PriceListDto(Long id) {
        this.id = id;
    }
}
