package com.cubex.promocube.core.dto.pricing;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter

@ApiModel
@NoArgsConstructor
public class PriceListValueDto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @ApiModelProperty(example = "2021-01-01", required = true)
    private Date dateBegin;

    @NotNull
    @ApiModelProperty(example = "1", required = true)
    private Long priceListId;

    @NotNull
    @ApiModelProperty(example = "1", required = true)
    private Long productId;

    @ApiModelProperty(example = "10", required = true)
    @NotNull
    private BigDecimal discountValue;

    public PriceListValueDto(long id) {
        this.id = id;
    }
}
