package com.cubex.promocube.core.dto.coupon;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor

@ApiModel
public class CouponSettingsDto {
    @ApiModelProperty(example = "1", required = true)
    @NotNull
    private Long id;

    @ApiModelProperty(example = "name", required = true)
    @NotBlank
    private String name;

    @NotNull
    @ApiModelProperty(example = "1", required = true)
    private Long actionId;

    @NotNull
    @ApiModelProperty(example = "2021-01-01", required = true)
    private Date dateBegin;

    @NotNull
    @ApiModelProperty(example = "2021-12-30", required = true)
    private Date dateEnd;

    public CouponSettingsDto(long id) {
        this.id = id;
    }
}
