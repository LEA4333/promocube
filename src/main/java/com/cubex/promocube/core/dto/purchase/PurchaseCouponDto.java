package com.cubex.promocube.core.dto.purchase;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString

@ApiModel
public class PurchaseCouponDto {
    @ApiModelProperty(example = "1", hidden = true)
    @NotNull
    private Long id;

    @NotNull
    @ApiModelProperty(example = "NO123456", required = true)
    private String coupon;

    @ApiModelProperty(example="false", hidden = true)
    private Boolean applying;
}
