package com.cubex.promocube.core.dto.purchase;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Setter
@ToString

@ApiModel
public class PurchaseCouponPromoDto {
    @ApiModelProperty(example = "1", hidden = true)
    @NotNull
    private Long id;

    @NotNull
    @ApiModelProperty(example = "NO123456", required = true)
    private String coupon;

    @NotNull
    @ApiModelProperty(example = "1", required = true)
    private Long actionId;

    @NotNull
    @ApiModelProperty(example = "1", required = true)
    private Long actionSettingsId;

    @NotNull
    @ApiModelProperty(example = "0", required = true)
    private BigDecimal sum;
}
