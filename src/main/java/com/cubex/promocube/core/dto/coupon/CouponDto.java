package com.cubex.promocube.core.dto.coupon;

import com.cubex.promocube.core.database.entity.coupon.CouponStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor

@ApiModel
public class CouponDto {
    @ApiModelProperty(example = "1", required = true)
    @NotNull
    private Long id;

    @ApiModelProperty(example = "123456", required = true)
    @NotBlank
    private String name;

    @NotNull
    @ApiModelProperty(example = "1", required = true)
    private Long сouponSettingsId;

    @ApiModelProperty(example = "1", required = true)
    @NotNull
    private CouponStatus couponStatus;

    public CouponDto(long l) {
    }
}
