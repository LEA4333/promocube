package com.cubex.promocube.core.dto.payment;

import com.cubex.promocube.core.database.entity.payment.PaymentType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@ApiModel
public class PaymentDto {
    @ApiModelProperty(example = "1", required = true)
    @NotNull
    private Long id;

    @ApiModelProperty(example = "Kaspi QR Gold", required = true)
    @NotBlank
    private String name;

    @ApiModelProperty(example = "1", required = true)
    @NotNull
    private PaymentType paymentType;

    public PaymentDto(Long id) {
        this.id = id;
    }
}
