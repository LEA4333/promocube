package com.cubex.promocube.core.dto.system;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Getter
@Setter
//@AllArgsConstructor
public class ApiErrorDto {
    private LocalDateTime timestamp;
    private String error;
    private Integer status;
    private String message;
    private Class exception;
    private String path;

    public ApiErrorDto(HttpStatus status) {
        this.status = status.value();
        this.error = status.getReasonPhrase();
        this.setTimestamp(LocalDateTime.now());
    }

    public ApiErrorDto(HttpStatus status,Exception exception) {
        this.status = status.value();
        this.error = status.getReasonPhrase();
        this.setTimestamp(LocalDateTime.now());

        this.setException(exception.getClass());
        this.setMessage(exception.getMessage());
    }
}
