package com.cubex.promocube.core.dto.purchase;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Setter
@ToString

@ApiModel
public class PurchaseProductDto {
    @ApiModelProperty(example = "1", hidden = true)
    @NotNull
    private Long id;

    @ApiModelProperty(example = "1", required = true)
    @NotNull
    private Long extId;

    @NotNull
    @ApiModelProperty(example = "1", required = true)
    private Long productId;

    @NotNull
    @ApiModelProperty(example = "500", required = true)
    private Long priceBase;

    @NotNull
    @ApiModelProperty(example = "500", required = true)
    private Long price;

    @NotNull
    @ApiModelProperty(example = "1", required = true)
    private Long count;

    @NotNull
    @ApiModelProperty(example = "500", required = true)
    private Long sum;

    @NotNull
    @ApiModelProperty(example = "0", required = true)
    private BigDecimal bonus;
}
