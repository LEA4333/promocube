package com.cubex.promocube.core.mapper;

import com.cubex.promocube.core.database.entity.productUnion.ProductUnionValue;
import com.cubex.promocube.core.domain.service.ProductService;
import com.cubex.promocube.core.domain.service.ProductUnionService;
import com.cubex.promocube.core.dto.productUnion.ProductUnionValueDto;
import com.cubex.promocube.core.exceptions.IdNotFoundMappingException;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class ProductUnionValueMapper extends AbstractMapper<ProductUnionValue, ProductUnionValueDto>{
    private final ModelMapper mapper;
    private final ProductUnionService productUnionService;
    private final ProductService productService;

    public ProductUnionValueMapper(ModelMapper mapper, ProductUnionService productUnionService, ProductService productService) {
        super(ProductUnionValue.class, ProductUnionValueDto.class);
        this.mapper = mapper;
        this.productUnionService = productUnionService;
        this.productService = productService;
    }

    @PostConstruct
    public void setupMapper() {
        mapper.createTypeMap(ProductUnionValue.class, ProductUnionValueDto.class)
                .addMappings(m -> m.skip(ProductUnionValueDto::setProductId))
                .addMappings(m -> m.skip(ProductUnionValueDto::setProductUnionId))
                .setPostConverter(toDtoConverter());

        mapper.createTypeMap(ProductUnionValueDto.class, ProductUnionValue.class)
                .addMappings(m -> m.skip(ProductUnionValue::setProduct))
                .addMappings(m -> m.skip(ProductUnionValue::setProductUnion))
                .setPostConverter(toEntityConverter());
    }

    @Override
    void mapSpecificFieldsEntity(ProductUnionValueDto source, ProductUnionValue destination) {
        destination.setProduct(productService.findById(source.getProductId()).orElse(null));
        destination.setProductUnion(productUnionService.findById(source.getProductUnionId()).orElse(null));

        if (destination.getProduct() == null){
            throw new IdNotFoundMappingException(source.getProductId(),"productId");
        }
        if (destination.getProductUnion() == null){
            throw new IdNotFoundMappingException(source.getProductUnionId(),"productUnionId");
        }
    }

    @Override
    void mapSpecificFieldsDto(ProductUnionValue source, ProductUnionValueDto destination) {
        destination.setProductId(productService.getId(source.getProduct()));
        destination.setProductUnionId(productUnionService.getId(source.getProductUnion()));
    }
}
