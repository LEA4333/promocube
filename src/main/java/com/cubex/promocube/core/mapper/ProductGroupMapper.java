package com.cubex.promocube.core.mapper;

import com.cubex.promocube.core.database.entity.product.ProductGroup;
import com.cubex.promocube.core.domain.service.ProductGroupService;
import com.cubex.promocube.core.dto.product.ProductGroupDto;
import com.cubex.promocube.core.exceptions.IdNotFoundMappingException;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Objects;
import java.util.Optional;

@Component
public class ProductGroupMapper extends AbstractMapper<ProductGroup, ProductGroupDto>{
    private final ModelMapper mapper;
    private final ProductGroupService productGroupService;

    public ProductGroupMapper(ModelMapper mapper, ProductGroupService productGroupService) {
        super(ProductGroup.class, ProductGroupDto.class);
        this.mapper = mapper;
        this.productGroupService = productGroupService;
    }

    @PostConstruct
    public void setupMapper() {
        mapper.createTypeMap(ProductGroup.class, ProductGroupDto.class)
                .addMappings(m -> m.skip(ProductGroupDto::setProductGroupId)).setPostConverter(toDtoConverter());

        mapper.createTypeMap(ProductGroupDto.class, ProductGroup.class)
                .addMappings(m -> m.skip(ProductGroup::setProductGroup)).setPostConverter(toEntityConverter());
    }

    public ProductGroup toEntity(ProductGroupDto dto){
        return Objects.isNull(dto)?null:mapper.map(dto, ProductGroup.class);
    }

    public ProductGroupDto toDto(ProductGroup entity) {
        return Objects.isNull(entity)?null:mapper.map(entity,ProductGroupDto.class);
    }

    @Override
    void mapSpecificFieldsEntity(ProductGroupDto source, ProductGroup destination) {
        if (source.getProductGroupId() != null){
            ProductGroup productGroup = productGroupService
                    .findById(source.getProductGroupId())
                    .orElseThrow(() -> new IdNotFoundMappingException(source.getProductGroupId(),"productGroupId"));
            destination.setProductGroup(productGroup);
        }
    }

    @Override
    void mapSpecificFieldsDto(ProductGroup source, ProductGroupDto destination) {
        Optional.ofNullable(source.getProductGroup())
                .ifPresent(x -> destination.setProductGroupId(x.getId()));
    }
}
