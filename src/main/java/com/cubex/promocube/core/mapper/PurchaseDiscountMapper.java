package com.cubex.promocube.core.mapper;

import com.cubex.promocube.core.database.entity.product.Product;
import com.cubex.promocube.core.database.entity.purchase.PurchaseDiscount;
import com.cubex.promocube.core.database.entity.purchase.PurchaseProduct;
import com.cubex.promocube.core.domain.service.ActionService;
import com.cubex.promocube.core.domain.service.ActionSettingsService;
import com.cubex.promocube.core.domain.service.ProductService;
import com.cubex.promocube.core.domain.service.PurchaseProductService;
import com.cubex.promocube.core.dto.purchase.PurchaseDiscountDto;
import com.cubex.promocube.core.dto.purchase.PurchaseProductDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Objects;

@Component
public class PurchaseDiscountMapper extends AbstractMapper<PurchaseDiscount, PurchaseDiscountDto>{
    private final ModelMapper mapper;

    private final ActionService actionService;
    private final ActionSettingsService actionSettingsService;
    private final PurchaseProductService purchaseProductService;

    @Autowired
    public PurchaseDiscountMapper(ModelMapper mapper, PurchaseProductService purchaseProductService, ActionService actionService, ActionSettingsService actionSettingsService) {
        super(PurchaseDiscount.class, PurchaseDiscountDto.class);
        this.mapper = mapper;
        this.purchaseProductService = purchaseProductService;
        this.actionService = actionService;
        this.actionSettingsService = actionSettingsService;
    }

    @PostConstruct
    public void setupMapper() {
        mapper.createTypeMap(PurchaseDiscount.class, PurchaseDiscountDto.class)
                .addMappings(m -> m.skip(PurchaseDiscountDto::setPurchaseProductId))
                .addMappings(m -> m.skip(PurchaseDiscountDto::setActionId))
                .addMappings(m -> m.skip(PurchaseDiscountDto::setActionSettingsId))
                .setPostConverter(toDtoConverter());

        mapper.createTypeMap(PurchaseDiscountDto.class, PurchaseDiscount.class)
                .addMappings(m -> m.skip(PurchaseDiscount::setPurchaseProduct))
                .addMappings(m -> m.skip(PurchaseDiscount::setAction))
                .addMappings(m -> m.skip(PurchaseDiscount::setActionSettings))
                .setPostConverter(toEntityConverter());

    }

    @Override
    void mapSpecificFieldsEntity(PurchaseDiscountDto source, PurchaseDiscount destination) {
        destination.setPurchaseProduct(purchaseProductService.findById(source.getPurchaseProductId()).orElse(null));
        destination.setAction(actionService.findById(source.getActionId()).orElse(null));
        destination.setActionSettings(actionSettingsService.findById(source.getActionSettingsId()).orElse(null));
    }

    @Override
    void mapSpecificFieldsDto(PurchaseDiscount source, PurchaseDiscountDto destination) {
        destination.setPurchaseProductId(purchaseProductService.getId(source.getPurchaseProduct()));
        destination.setActionId(actionService.getId(source.getAction()));
        destination.setActionSettingsId(actionSettingsService.getId(source.getActionSettings()));
    }
}
