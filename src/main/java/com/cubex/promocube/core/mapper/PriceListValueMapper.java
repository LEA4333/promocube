package com.cubex.promocube.core.mapper;

import com.cubex.promocube.core.database.entity.pricing.PriceListValue;
import com.cubex.promocube.core.domain.service.PriceListService;
import com.cubex.promocube.core.domain.service.ProductService;
import com.cubex.promocube.core.dto.pricing.PriceListValueDto;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Optional;

@Component
public class PriceListValueMapper extends AbstractMapper<PriceListValue, PriceListValueDto>{
    private final ModelMapper mapper;
    private final PriceListService priceListService;
    private final ProductService productService;

    public PriceListValueMapper(ModelMapper mapper, PriceListService priceListService, ProductService productService) {
        super(PriceListValue.class, PriceListValueDto.class);
        this.mapper = mapper;
        this.priceListService = priceListService;
        this.productService = productService;
    }

    @PostConstruct
    public void setupMapper() {
        mapper.createTypeMap(PriceListValue.class, PriceListValueDto.class)
                .addMappings(m -> m.skip(PriceListValueDto::setProductId))
                .addMappings(m -> m.skip(PriceListValueDto::setPriceListId))
                .setPostConverter(toDtoConverter());

        mapper.createTypeMap(PriceListValueDto.class, PriceListValue.class)
                .addMappings(m -> m.skip(PriceListValue::setProduct))
                .addMappings(m -> m.skip(PriceListValue::setPriceList))
                .setPostConverter(toEntityConverter());
    }

    @Override
    void mapSpecificFieldsEntity(PriceListValueDto source, PriceListValue destination) {
        destination.setProduct(productService.findById(source.getProductId()).orElse(null));
        destination.setPriceList(priceListService.findById(source.getPriceListId()).orElse(null));
    }

    @Override
    void mapSpecificFieldsDto(PriceListValue source, PriceListValueDto destination) {
        destination.setProductId(productService.getId(source.getProduct()));
        destination.setPriceListId(priceListService.getId(source.getPriceList()));
    }
}
