package com.cubex.promocube.core.mapper;

import com.cubex.promocube.core.database.entity.coupon.Coupon;
import com.cubex.promocube.core.database.entity.coupon.CouponSettings;
import com.cubex.promocube.core.domain.service.ActionService;
import com.cubex.promocube.core.domain.service.CouponSettingsService;
import com.cubex.promocube.core.dto.coupon.CouponDto;
import com.cubex.promocube.core.dto.coupon.CouponSettingsDto;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Objects;

@Component
public class CouponMapper extends AbstractMapper<Coupon, CouponDto>{
    private final ModelMapper mapper;
    private final CouponSettingsService couponSettingsService;

    public CouponMapper(ModelMapper mapper, CouponSettingsService couponSettingsService) {
        super(Coupon.class, CouponDto.class);
        this.mapper = mapper;
        this.couponSettingsService = couponSettingsService;
    }

    @PostConstruct
    public void setupMapper() {
        mapper.createTypeMap(CouponDto.class, Coupon.class)
                .addMappings(m -> m.skip(Coupon::setCouponSettings))
                .setPostConverter(toEntityConverter());

        mapper.createTypeMap(Coupon.class, CouponDto.class)
                .addMappings(m -> m.skip(CouponDto::setСouponSettingsId))
                .setPostConverter(toDtoConverter());
    }

    @Override
    void mapSpecificFieldsEntity(CouponDto source, Coupon destination) {
        if (source.getСouponSettingsId() != null) {
            destination.setCouponSettings(couponSettingsService.findById(source.getСouponSettingsId()).orElse(null));
        }
    }

    @Override
    void mapSpecificFieldsDto(Coupon source, CouponDto destination) {
        if (source.getCouponSettings() != null) {
            destination.setСouponSettingsId(source.getCouponSettings().getId());
        }
    }

    public Coupon toEntity(CouponDto dto){
        return Objects.isNull(dto)?null:mapper.map(dto, Coupon.class);
    }

    public CouponDto toDto(Coupon action) {
        return Objects.isNull(action)?null:mapper.map(action,CouponDto.class);
    }

}
