package com.cubex.promocube.core.mapper;

import com.cubex.promocube.core.database.entity.user.UserRole;
import com.cubex.promocube.core.dto.user.UserRoleDto;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class UserRoleMapper {
    private final ModelMapper mapper;

    public UserRoleMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    public UserRole toEntity(UserRoleDto dto){
        return Objects.isNull(dto)?null:mapper.map(dto,UserRole.class);
    }

    public UserRoleDto toDto(UserRole item) {
        return Objects.isNull(item)?null:mapper.map(item,UserRoleDto.class);
    }
}
