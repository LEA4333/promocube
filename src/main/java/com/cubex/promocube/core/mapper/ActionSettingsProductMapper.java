package com.cubex.promocube.core.mapper;

import com.cubex.promocube.core.database.entity.actionsettings.ActionSettingsProduct;
import com.cubex.promocube.core.database.entity.product.Product;
import com.cubex.promocube.core.domain.service.ProductService;
import com.cubex.promocube.core.dto.actionsettigs.ActionSettingsProductDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Objects;

@Component
class ActionSettingsProductMapper extends AbstractMapper<ActionSettingsProduct, ActionSettingsProductDto>{
    private final ModelMapper mapper;
    private final ProductService productService;

    @Autowired
    public ActionSettingsProductMapper(ModelMapper mapper, ProductService productService) {
        super(ActionSettingsProduct.class, ActionSettingsProductDto.class);
        this.mapper = mapper;
        this.productService = productService;
    }

    @PostConstruct
    public void setupMapper() {
        mapper.createTypeMap(ActionSettingsProduct.class, ActionSettingsProductDto.class)
                .addMappings(m -> m.skip(ActionSettingsProductDto::setProductId)).setPostConverter(toDtoConverter());

        mapper.createTypeMap(ActionSettingsProductDto.class, ActionSettingsProduct.class)
                .addMappings(m -> m.skip(ActionSettingsProduct::setProduct)).setPostConverter(toEntityConverter());
    }

    @Override
    void mapSpecificFieldsEntity(ActionSettingsProductDto source, ActionSettingsProduct destination) {
        destination.setProduct(productService.findById(source.getProductId()).orElse(null));
    }

    @Override
    void mapSpecificFieldsDto(ActionSettingsProduct source, ActionSettingsProductDto destination) {
        destination.setProductId(getId(source.getProduct()));
    }

    private Long getId(Product product) {
        return Objects.isNull(product) ? null : product.getId();
    }
}
