package com.cubex.promocube.core.mapper;

import com.cubex.promocube.core.database.entity.actionsettings.ActionSettings;
import com.cubex.promocube.core.database.entity.actionsettings.ActionSettingsCondition;
import com.cubex.promocube.core.database.entity.actionsettings.ActionSettingsProduct;
import com.cubex.promocube.core.domain.service.ActionService;
import com.cubex.promocube.core.domain.service.PriceListService;
import com.cubex.promocube.core.dto.actionsettigs.ActionSettingsDto;
import com.cubex.promocube.core.dto.actionsettigs.ActionSettingsProductDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class ActionSettingsMapper extends AbstractMapper<ActionSettings, ActionSettingsDto>{
    private final ModelMapper mapper;
    private final ActionService actionService;
    private final PriceListService priceListService;

    @Autowired
    public ActionSettingsMapper(ModelMapper mapper, ActionService actionService,  PriceListService priceListService) {
        super(ActionSettings.class, ActionSettingsDto.class);
        this.mapper = mapper;
        this.actionService = actionService;
        this.priceListService = priceListService;
    }

    @PostConstruct
    public void setupMapper() {
        mapper.createTypeMap(ActionSettingsDto.class, ActionSettings.class)
                .addMappings(m -> m.skip(ActionSettings::setAction))
                .addMappings(m -> m.skip(ActionSettings::setPriceList)).
                setPostConverter(toEntityConverter());

        mapper.createTypeMap(ActionSettings.class, ActionSettingsDto.class)
                .addMappings(m -> m.skip(ActionSettingsDto::setActionId))
                .addMappings(m -> m.skip(ActionSettingsDto::setPriceListId))
                .setPostConverter(toDtoConverter());
    }

    @Override
    void mapSpecificFieldsEntity(ActionSettingsDto source, ActionSettings destination) {
        if(source.getActionId()!=null){
            destination.setAction(actionService.findById(source.getActionId()).orElse(null));
        }

        if(source.getPriceListId()!=null){
            destination.setPriceList(priceListService.findById(source.getPriceListId()).orElse(null));
        }

        if(destination.getAction()!=null){
            destination.setMechanicsType(destination.getAction().getMechanicsType());
        }

        for (ActionSettingsCondition condition: destination.getConditions()){
            for (ActionSettingsProduct product: condition.getProducts()){
                product.setCondition(condition);
            }
        }
    }


    @Override
    void mapSpecificFieldsDto(ActionSettings source, ActionSettingsDto destination) {
        if(source.getAction()!=null){
            destination.setActionId(source.getAction().getId());
        }

        if(source.getPriceList()!=null){
            destination.setPriceListId(source.getPriceList().getId());
        }
    }
}
