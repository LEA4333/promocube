package com.cubex.promocube.core.mapper;

import com.cubex.promocube.core.database.entity.actionsettings.ActionSettingsProductGroup;
import com.cubex.promocube.core.database.entity.product.ProductGroup;
import com.cubex.promocube.core.domain.service.ProductGroupService;
import com.cubex.promocube.core.dto.actionsettigs.ActionSettingsProductGroupDto;
import com.cubex.promocube.core.exceptions.IdNotFoundMappingException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Optional;

@Component
class ActionSettingsProductGroupMapper extends AbstractMapper<ActionSettingsProductGroup, ActionSettingsProductGroupDto>{
    private final ModelMapper mapper;
    private final ProductGroupService productGroupService;

    @Autowired
    public ActionSettingsProductGroupMapper(ModelMapper mapper, ProductGroupService productGroupService) {
        super(ActionSettingsProductGroup.class, ActionSettingsProductGroupDto.class);
        this.mapper = mapper;
        this.productGroupService = productGroupService;
    }

    @PostConstruct
    public void setupMapper() {
        mapper.createTypeMap(ActionSettingsProductGroup.class, ActionSettingsProductGroupDto.class)
                .addMappings(m -> m.skip(ActionSettingsProductGroupDto::setProductGroupId)).setPostConverter(toDtoConverter());

        mapper.createTypeMap(ActionSettingsProductGroupDto.class, ActionSettingsProductGroup.class)
                .addMappings(m -> m.skip(ActionSettingsProductGroup::setProductGroup)).setPostConverter(toEntityConverter());
    }

    @Override
    void mapSpecificFieldsEntity(ActionSettingsProductGroupDto source, ActionSettingsProductGroup destination) {
        if (source.getProductGroupId() != null){
            ProductGroup productGroup = productGroupService
                    .findById(source.getProductGroupId())
                    .orElseThrow(() -> new IdNotFoundMappingException(source.getProductGroupId(),"productGroupId"));
            destination.setProductGroup(productGroup);
        }
    }

    @Override
    void mapSpecificFieldsDto(ActionSettingsProductGroup source, ActionSettingsProductGroupDto destination) {
        Optional.ofNullable(source.getProductGroup())
                .ifPresent(x -> destination.setProductGroupId(x.getId()));
    }
}
