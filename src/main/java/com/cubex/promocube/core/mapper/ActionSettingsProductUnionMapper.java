package com.cubex.promocube.core.mapper;

import com.cubex.promocube.core.database.entity.actionsettings.ActionSettingsProductUnion;
import com.cubex.promocube.core.database.entity.product.ProductUnion;
import com.cubex.promocube.core.domain.service.ProductUnionService;
import com.cubex.promocube.core.dto.actionsettigs.ActionSettingsProductUnionDto;
import com.cubex.promocube.core.exceptions.IdNotFoundMappingException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Optional;

@Component
class ActionSettingsProductUnionMapper extends AbstractMapper<ActionSettingsProductUnion, ActionSettingsProductUnionDto>{
    private final ModelMapper mapper;
    private final ProductUnionService productUnionService;

    @Autowired
    public ActionSettingsProductUnionMapper(ModelMapper mapper, ProductUnionService productUnionService) {
        super(ActionSettingsProductUnion.class, ActionSettingsProductUnionDto.class);
        this.mapper = mapper;
        this.productUnionService = productUnionService;
    }

    @PostConstruct
    public void setupMapper() {
        mapper.createTypeMap(ActionSettingsProductUnion.class, ActionSettingsProductUnionDto.class)
                .addMappings(m -> m.skip(ActionSettingsProductUnionDto::setProductUnionId)).setPostConverter(toDtoConverter());

        mapper.createTypeMap(ActionSettingsProductUnionDto.class, ActionSettingsProductUnion.class)
                .addMappings(m -> m.skip(ActionSettingsProductUnion::setProductUnion)).setPostConverter(toEntityConverter());
    }

    @Override
    void mapSpecificFieldsEntity(ActionSettingsProductUnionDto source, ActionSettingsProductUnion destination) {
        if (source.getProductUnionId() != null){
            ProductUnion productUnion = productUnionService
                    .findById(source.getProductUnionId())
                    .orElseThrow(() -> new IdNotFoundMappingException(source.getProductUnionId(),"productUnionId"));
            destination.setProductUnion(productUnion);
        }
    }

    @Override
    void mapSpecificFieldsDto(ActionSettingsProductUnion source, ActionSettingsProductUnionDto destination) {
        Optional.ofNullable(source.getProductUnion())
                .ifPresent(x -> destination.setProductUnionId(x.getId()));
    }
}
