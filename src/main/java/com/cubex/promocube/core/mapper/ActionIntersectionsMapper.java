package com.cubex.promocube.core.mapper;

import com.cubex.promocube.core.database.entity.actionsettings.ActionSettings;
import com.cubex.promocube.core.database.entity.actionsettings.ActionSettingsCondition;
import com.cubex.promocube.core.database.entity.actionsettings.ActionSettingsProduct;
import com.cubex.promocube.core.database.entity.intersection.ActionIntersection;
import com.cubex.promocube.core.domain.service.ActionService;
import com.cubex.promocube.core.domain.service.PriceListService;
import com.cubex.promocube.core.dto.actionsettigs.ActionSettingsDto;
import com.cubex.promocube.core.dto.intersection.ActionIntersectionDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class ActionIntersectionsMapper extends AbstractMapper<ActionIntersection, ActionIntersectionDto>{
    private final ModelMapper mapper;
    private final ActionService actionService;

    @Autowired
    public ActionIntersectionsMapper(ModelMapper mapper, ActionService actionService) {
        super(ActionIntersection.class, ActionIntersectionDto.class);
        this.mapper = mapper;
        this.actionService = actionService;
    }

    @PostConstruct
    public void setupMapper() {
        mapper.createTypeMap(ActionIntersectionDto.class, ActionIntersection.class)
                .addMappings(m -> m.skip(ActionIntersection::setAction))
                .addMappings(m -> m.skip(ActionIntersection::setActionCross)).
                setPostConverter(toEntityConverter());

        mapper.createTypeMap(ActionIntersection.class, ActionIntersectionDto.class)
                .addMappings(m -> m.skip(ActionIntersectionDto::setActionId))
                .addMappings(m -> m.skip(ActionIntersectionDto::setActionCrossId))
                .setPostConverter(toDtoConverter());
    }

    @Override
    void mapSpecificFieldsEntity(ActionIntersectionDto source, ActionIntersection destination) {
        if(source.getActionId()!=null){
            destination.setAction(actionService.findById(source.getActionId()).orElse(null));
        }

        if(source.getActionCrossId()!=null){
            destination.setActionCross(actionService.findById(source.getActionCrossId()).orElse(null));
        }
    }

    @Override
    void mapSpecificFieldsDto(ActionIntersection source, ActionIntersectionDto destination) {
        if(source.getAction()!=null){
            destination.setActionId(source.getAction().getId());
        }

        if(source.getActionCross()!=null){
            destination.setActionCrossId(source.getActionCross().getId());
        }
    }
}
