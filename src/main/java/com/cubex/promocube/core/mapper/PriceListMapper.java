package com.cubex.promocube.core.mapper;

import com.cubex.promocube.core.database.entity.pricing.PriceList;
import com.cubex.promocube.core.dto.pricing.PriceListDto;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class PriceListMapper {
    private final ModelMapper mapper;

    public PriceListMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    public PriceList toEntity(PriceListDto dto){
        return Objects.isNull(dto)?null:mapper.map(dto,PriceList.class);
    }

    public PriceListDto toDto(PriceList item) {
        return Objects.isNull(item)?null:mapper.map(item,PriceListDto.class);
    }
}
