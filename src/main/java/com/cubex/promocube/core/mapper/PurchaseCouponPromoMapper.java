package com.cubex.promocube.core.mapper;

import com.cubex.promocube.core.database.entity.purchase.PurchaseCoupon;
import com.cubex.promocube.core.database.entity.purchase.PurchaseCouponPromo;
import com.cubex.promocube.core.domain.service.ActionService;
import com.cubex.promocube.core.domain.service.ActionSettingsService;
import com.cubex.promocube.core.domain.service.CouponService;
import com.cubex.promocube.core.dto.purchase.PurchaseCouponDto;
import com.cubex.promocube.core.dto.purchase.PurchaseCouponPromoDto;
import com.cubex.promocube.core.exceptions.CouponNotFoundException;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class PurchaseCouponPromoMapper extends AbstractMapper<PurchaseCouponPromo, PurchaseCouponPromoDto>{
    private final ModelMapper mapper;
    private final CouponService couponService;
    private final ActionService actionService;
    private final ActionSettingsService actionSettingsService;

    @Autowired
    public PurchaseCouponPromoMapper(ModelMapper mapper, CouponService couponService, ActionService actionService, ActionSettingsService actionSettingsService) {
        super(PurchaseCouponPromo.class, PurchaseCouponPromoDto.class);
        this.mapper = mapper;
        this.couponService = couponService;
        this.actionService = actionService;
        this.actionSettingsService = actionSettingsService;
    }

    @PostConstruct
    public void setupMapper() {
        mapper.createTypeMap(PurchaseCouponPromo.class, PurchaseCouponPromoDto.class)
                .addMappings(m -> m.skip(PurchaseCouponPromoDto::setCoupon))
                .setPostConverter(toDtoConverter());

        mapper.createTypeMap(PurchaseCouponPromoDto.class, PurchaseCouponPromo.class)
                .addMappings(m -> m.skip(PurchaseCouponPromo::setCoupon))
                .setPostConverter(toEntityConverter());

        mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

    }

    @Override
    void mapSpecificFieldsEntity(PurchaseCouponPromoDto source, PurchaseCouponPromo destination) {
        destination.setCoupon(couponService.findByName(source.getCoupon()).orElse(null));
        destination.setAction(actionService.findById(source.getActionId()).orElse(null));
        destination.setActionSettings(actionSettingsService.findById(source.getActionSettingsId()).orElse(null));
    }

    @Override
    void mapSpecificFieldsDto(PurchaseCouponPromo source, PurchaseCouponPromoDto destination) {
        if (source.getCoupon() != null) {
            destination.setCoupon(source.getCoupon().getName());
        }

        destination.setActionId(actionService.getId(source.getAction()));
        destination.setActionSettingsId(actionSettingsService.getId(source.getActionSettings()));
    }
}
