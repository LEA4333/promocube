package com.cubex.promocube.core.mapper;

import com.cubex.promocube.core.database.entity.actionsettings.ActionSettingsProduct;
import com.cubex.promocube.core.database.entity.product.Product;
import com.cubex.promocube.core.database.entity.purchase.Purchase;
import com.cubex.promocube.core.database.entity.purchase.PurchaseProduct;
import com.cubex.promocube.core.database.entity.user.UserRole;
import com.cubex.promocube.core.domain.service.DepartmentService;
import com.cubex.promocube.core.domain.service.PaymentService;
import com.cubex.promocube.core.domain.service.UserRoleService;
import com.cubex.promocube.core.dto.actionsettigs.ActionSettingsProductDto;
import com.cubex.promocube.core.dto.purchase.PurchaseDto;
import com.cubex.promocube.core.dto.purchase.PurchaseProductDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Objects;

@Component
public class PurchaseMapper extends AbstractMapper<Purchase, PurchaseDto>{
    private final ModelMapper mapper;
    private final UserRoleService userRoleService;
    private final PaymentService paymentService;
    private final DepartmentService departmentService;

    public PurchaseMapper(ModelMapper mapper, UserRoleService userRoleService, PaymentService paymentService, DepartmentService departmentService) {
        super(Purchase.class, PurchaseDto.class);
        this.mapper = mapper;
        this.userRoleService = userRoleService;
        this.paymentService = paymentService;
        this.departmentService = departmentService;
    }

    @PostConstruct
    public void setupMapper() {
        mapper.createTypeMap(Purchase.class, PurchaseDto.class)
                .addMappings(m -> m.skip(PurchaseDto::setUserRoleId))
                .addMappings(m -> m.skip(PurchaseDto::setPaymentId))
                .addMappings(m -> m.skip(PurchaseDto::setDepartmentId))
                .setPostConverter(toDtoConverter());

        mapper.createTypeMap(PurchaseDto.class, Purchase.class)
                .addMappings(m -> m.skip(Purchase::setUserRole))
                .addMappings(m -> m.skip(Purchase::setPayment))
                .addMappings(m -> m.skip(Purchase::setDepartment))
                .setPostConverter(toEntityConverter());
    }

    @Override
    void mapSpecificFieldsEntity(PurchaseDto source, Purchase destination) {
        destination.setUserRole(userRoleService.findById(source.getUserRoleId()).orElse(null));
        destination.setPayment(paymentService.findById(source.getPaymentId()).orElse(null));
        destination.setDepartment(departmentService.findById(source.getDepartmentId()).orElse(null));
    }

    @Override
    void mapSpecificFieldsDto(Purchase source, PurchaseDto destination) {
        destination.setUserRoleId(userRoleService.getId(source.getUserRole()));
        destination.setPaymentId(paymentService.getId(source.getPayment()));
        destination.setDepartmentId(departmentService.getId(source.getDepartment()));
    }
}
