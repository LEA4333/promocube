package com.cubex.promocube.core.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class BigDecimalUtil {
    public static BigDecimal percentage(BigDecimal base, BigDecimal pct){
        return base.multiply(pct).divide(new BigDecimal(100)).setScale(2,RoundingMode.HALF_EVEN);
    }
}
