package com.cubex.promocube.core.processing.purchase;

import com.cubex.promocube.core.database.entity.action.ActionMechanicsType;

import java.util.ArrayList;
import java.util.List;

public class DiscountForPositionSetAlgorithm extends AlgorithmPrototype {

    @Override
    public boolean canHandleRequest(AlgorithmRequest request) {
        if(!super.canHandleRequest(request)) return false;

        List<ActionMechanicsType> actionMechanicsTypes = new ArrayList<>();
        actionMechanicsTypes.add(ActionMechanicsType.DISCOUNT_FOR_PRODUCTS_SET);
        actionMechanicsTypes.add(ActionMechanicsType.DISCOUNT_FOR_PRODUCTS_CASCADE);
        actionMechanicsTypes.add(ActionMechanicsType.PRICE_FOR_SERVICES);

        return actionMechanicsTypes.contains(request.getMechanicsType());
    }

    @Override
    void execute(AlgorithmRequest request) {

    }
}
