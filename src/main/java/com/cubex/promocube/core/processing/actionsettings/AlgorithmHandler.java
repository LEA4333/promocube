package com.cubex.promocube.core.processing.actionsettings;

public interface AlgorithmHandler {
    AlgorithmHandler setNext(AlgorithmHandler next);
    void handle (AlgorithmRequest request);
    boolean canHandleRequest(AlgorithmRequest request);
}
