package com.cubex.promocube.core.processing;

import com.cubex.promocube.core.database.entity.coupon.Coupon;
import com.cubex.promocube.core.database.entity.coupon.CouponStatus;
import com.cubex.promocube.core.database.entity.purchase.Purchase;
import com.cubex.promocube.core.database.entity.purchase.PurchaseCoupon;
import com.cubex.promocube.core.domain.service.CouponService;
import com.cubex.promocube.core.exceptions.CouponApplayingErrorException;
import com.cubex.promocube.core.exceptions.CouponNotAppropriateException;
import org.springframework.stereotype.Service;

@Service
public class CouponProcessing {
    private CouponService couponService;

    public CouponProcessing(CouponService couponService) {
        this.couponService = couponService;
    }

    public void execute(Purchase purchase){
        for (PurchaseCoupon purchaseCoupon : purchase.getCoupons()){
            if(purchaseCoupon.getApplying()){
                Coupon coupon = purchaseCoupon.getCoupon();
                if(!couponService.changeCouponStatus(coupon, CouponStatus.ACTIVATED, CouponStatus.USED)){
                    throw new CouponApplayingErrorException(coupon);
                };
            }else {
                throw new CouponNotAppropriateException(purchaseCoupon.getCoupon());
            }
        }
    }
}
