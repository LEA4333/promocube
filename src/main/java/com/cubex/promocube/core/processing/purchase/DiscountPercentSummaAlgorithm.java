package com.cubex.promocube.core.processing.purchase;

import com.cubex.promocube.core.database.entity.action.ActionMechanicsType;
import com.cubex.promocube.core.database.entity.actionsettings.ActionSettings;
import com.cubex.promocube.core.database.entity.actionsettings.ActionSettingsCondition;
import com.cubex.promocube.core.database.entity.actionsettings.DiscountPayoutType;
import com.cubex.promocube.core.database.entity.actionsettings.OperationType;
import com.cubex.promocube.core.database.entity.intersection.ActionIntersection;
import com.cubex.promocube.core.database.entity.purchase.Purchase;
import com.cubex.promocube.core.database.entity.purchase.PurchaseDiscount;
import com.cubex.promocube.core.database.entity.purchase.PurchaseProduct;
import com.cubex.promocube.core.util.BigDecimalUtil;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

@Component
public class DiscountPercentSummaAlgorithm extends AlgorithmPrototype {

    public boolean canHandleRequest(AlgorithmRequest request){
        if(!super.canHandleRequest(request)) return false;

        List<ActionMechanicsType> actionMechanicsTypes = new ArrayList<>();
        actionMechanicsTypes.add(ActionMechanicsType.DISCOUNT_PERCENT_SUMMA);
        actionMechanicsTypes.add(ActionMechanicsType.DISCOUNT_BY_PRICE_LIST);
        actionMechanicsTypes.add(ActionMechanicsType.BONUS_PERCENT_SUMMA);

        return actionMechanicsTypes.contains(request.getMechanicsType());
    }

    public void execute(AlgorithmRequest request){
        ActionSettings setting = request.getActionSettings();
        Purchase purchase = request.getPurchase();
        List<ActionIntersection> intersections = request.getActionIntersections();

        for(ActionSettingsCondition actionCondition : setting.getConditions()){
            for(PurchaseProduct purchaseProduct : purchase.getProducts()){
                if(isActionApplying(setting.getAction(), intersections, purchase, purchaseProduct) &&
                        isConditionApplying(actionCondition, purchaseProduct.getProduct())){
                    applyOperation(actionCondition, setting, purchaseProduct, purchase);
                }
            }
        }
    }

    void applyOperation(ActionSettingsCondition actionCondition, ActionSettings setting, PurchaseProduct purchaseProduct, Purchase purchase){

        BigDecimal discount = new BigDecimal(0);

        if(actionCondition.getOperationType()== OperationType.SUMMA) {
            discount = actionCondition.getDiscountValue();
        }
        else if (actionCondition.getOperationType()==OperationType.PERCENT) {
            discount = BigDecimalUtil.percentage(purchaseProduct.getPriceBase(), actionCondition.getDiscountValue());
        }
        else if (actionCondition.getOperationType()==OperationType.PRICE){
            if(purchaseProduct.getPriceBase().compareTo(actionCondition.getDiscountValue())>0){
                discount = purchaseProduct.getPriceBase().subtract(actionCondition.getDiscountValue());
            }
        }

        applyPurchaseDiscount(purchase, purchaseProduct, discount, setting);
    }
}
