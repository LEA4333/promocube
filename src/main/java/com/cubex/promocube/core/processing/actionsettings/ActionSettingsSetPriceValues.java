package com.cubex.promocube.core.processing.actionsettings;

import com.cubex.promocube.core.database.entity.action.ActionMechanicsType;
import com.cubex.promocube.core.database.entity.actionsettings.ActionSettings;
import com.cubex.promocube.core.database.entity.actionsettings.ActionSettingsCondition;
import com.cubex.promocube.core.database.entity.actionsettings.ActionSettingsProduct;
import com.cubex.promocube.core.database.entity.pricing.PriceListValue;
import com.cubex.promocube.core.database.entity.product.Product;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class ActionSettingsSetPriceValues extends AlgorithmPrototype{
    @Override
    public boolean canHandleRequest(AlgorithmRequest request) {
        if(!super.canHandleRequest(request)) return false;

        List<ActionMechanicsType> actionMechanicsTypes = new ArrayList<>();
        actionMechanicsTypes.add(ActionMechanicsType.DISCOUNT_BY_PRICE_LIST);

        if(!actionMechanicsTypes.contains(request.getMechanicsType())) return false;

        if(request.getActionSettings().getPriceList()==null) return false;

        return true;
    }

    @Override
    void execute(AlgorithmRequest request) {
        ActionSettings setting = request.getActionSettings();
        List<PriceListValue> priceListValues = request.getPriceListValues();
        List<Product> products = request.getProducts();

        List<ActionSettingsCondition> conditionList = new ArrayList<>();

        for(ActionSettingsCondition actionSettingsCondition : setting.getConditions()){
            for(ActionSettingsProduct actionSettingsProduct : actionSettingsCondition.getProducts()){
                if(products.contains(actionSettingsProduct.getProduct())){
                    Optional<PriceListValue> priceListValue = request.getPriceListValue(setting.getPriceList(), actionSettingsProduct.getProduct());
                    if (!priceListValue.isPresent()) continue;

                    ActionSettingsCondition conditionNew = new ActionSettingsCondition();
                    conditionNew.setOperationType(actionSettingsCondition.getOperationType());
                    conditionNew.setDiscountValue(priceListValue.get().getDiscountValue());

                    ActionSettingsProduct productNew = new ActionSettingsProduct();
                    productNew.setProduct(actionSettingsProduct.getProduct());
                    productNew.setCondition(conditionNew);

                    conditionNew.setProducts(Collections.singletonList(productNew));
                    conditionList.add(conditionNew);
                }
            }
        }

        setting.setConditions(conditionList);

    }
}
