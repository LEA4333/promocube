package com.cubex.promocube.core.processing.purchase;

public interface AlgorithmHandler {
    AlgorithmHandler setNext(AlgorithmHandler next);
    void handle (AlgorithmRequest request);

}
