package com.cubex.promocube.core.processing.purchase;

import com.cubex.promocube.core.database.entity.action.ActionMechanicsType;
import com.cubex.promocube.core.database.entity.actionsettings.ActionSettings;
import com.cubex.promocube.core.database.entity.intersection.ActionIntersection;
import com.cubex.promocube.core.database.entity.purchase.Purchase;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor

public class AlgorithmRequest {
    private Purchase purchase;
    private ActionSettings actionSettings;
    private List<ActionIntersection> actionIntersections;

    public AlgorithmRequest(Purchase purchase) {
        this.purchase = purchase;
    }

    public ActionMechanicsType getMechanicsType(){
        return actionSettings.getMechanicsType();
    }
}
