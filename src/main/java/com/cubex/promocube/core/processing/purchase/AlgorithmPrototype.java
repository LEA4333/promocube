package com.cubex.promocube.core.processing.purchase;

import com.cubex.promocube.core.database.entity.action.Action;
import com.cubex.promocube.core.database.entity.actionsettings.*;
import com.cubex.promocube.core.database.entity.coupon.Coupon;
import com.cubex.promocube.core.database.entity.intersection.ActionIntersection;
import com.cubex.promocube.core.database.entity.payment.Payment;
import com.cubex.promocube.core.database.entity.product.Product;
import com.cubex.promocube.core.database.entity.purchase.Purchase;
import com.cubex.promocube.core.database.entity.purchase.PurchaseCoupon;
import com.cubex.promocube.core.database.entity.purchase.PurchaseDiscount;
import com.cubex.promocube.core.database.entity.purchase.PurchaseProduct;
import com.cubex.promocube.core.database.entity.user.UserRole;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AlgorithmPrototype implements AlgorithmHandler {
    AlgorithmHandler nextHandler;

    @Override
    public void handle(AlgorithmRequest request){
        if(canHandleRequest(request)){
            execute(request);
        } else if(nextHandler!=null){
            nextHandler.handle(request);
        }
    }

    @Override
    public AlgorithmHandler setNext(AlgorithmHandler next) {
        nextHandler = next;
        return nextHandler;
    }

    public boolean canHandleRequestPrototype(AlgorithmRequest request) {
        ActionSettings actionSettings = request.getActionSettings();
        Purchase purchase = request.getPurchase();

        if (!isPaymentApplying(actionSettings,purchase.getPayment())){
            return false;
        }

        if (!isUserRoleApplying(actionSettings,purchase.getUserRole())){
            return false;
        }

        if (!isCouponsApplying(actionSettings,purchase.getCoupons())){
            return false;
        }

        return true;
    }

    public boolean canHandleRequest(AlgorithmRequest request) {
        if(!canHandleRequestPrototype(request)) return false;

        return true;
    }

    abstract void execute(AlgorithmRequest request);

    boolean isActionApplying(Action action, List<ActionIntersection> intersections, Purchase purchase, PurchaseProduct purchaseProduct){
        List<PurchaseDiscount> discounts = purchase.getDiscounts().stream()
                .filter(x -> !x.getAction().equals(action) && purchaseProduct.equals(purchaseProduct))
                .collect(Collectors.toList());

        List<Action> actions = discounts.stream().map(PurchaseDiscount::getAction)
                .collect(Collectors.toList());

        //Discounts is empty, it can apply any action
        if(discounts.isEmpty()){
            return true;
        }

        //By defalt action don't wotk with another action
        if(intersections.isEmpty()){
            return false;
        }

        Optional<ActionIntersection> intersectionSwitchOnALL = intersections.stream().filter(x -> (x.isActivatedALL(action))).findAny();
        List<ActionIntersection> intersectionsSwitchOff = intersections.stream().filter(x -> (x.isDeactivated(action) && actions.contains(x.getActionCross()))).collect(Collectors.toList());
        List<ActionIntersection> intersectionsSwitchOn = intersections.stream().filter(x -> x.isActivated(action)).collect(Collectors.toList());

        //Action can work with all another action
        if (!intersectionSwitchOnALL.isEmpty() && intersectionsSwitchOff.isEmpty()) return true;

        if(intersectionsSwitchOn.isEmpty()) return false;

        //Action can work with chosen actions
        for (PurchaseDiscount discount : discounts) {
            Optional<ActionIntersection> intersectionOptional = intersectionsSwitchOn.stream()
                    .filter(x -> x.getAction().equals(discount.getAction()) ||
                            x.getActionCross().equals(discount.getAction())).findAny();
            if (intersectionOptional.isEmpty()) {
                return false;
            }
        }

        return true;
    }

    boolean isCouponsApplying(ActionSettings actionSettings, List<PurchaseCoupon> coupons){
        if(!actionSettings.getIsCouponRequired()){
            return true;
        }

        if(coupons.isEmpty()){
            return false;
        }

        for(PurchaseCoupon purchaseCoupon:coupons){
            if(purchaseCoupon.getApplying()) continue;

            Coupon coupon = purchaseCoupon.getCoupon();
            Action action = (coupon!=null?coupon.getCouponSettings().getAction():null);
            if(coupon==null || action==null) continue;

            if(actionSettings.getAction().equals(action)){
                purchaseCoupon.setApplying(true);
                purchaseCoupon.setAction(action);
                purchaseCoupon.setActionSettings(actionSettings);
                return true;
            }
        }

        return false;
    }

    boolean isUserRoleApplying(ActionSettings actionSettings, UserRole userRole){
        if (actionSettings.getUserRoles().isEmpty() ){
            return true;
        }

        if(userRole==null) {
            return false;
        }

        Optional<ActionSettingsUserRole> optional = actionSettings.getUserRoles().stream()
                .filter(x->x.getUserRole().equals(userRole)).findAny();

        return optional.isPresent();
    }

    boolean isConditionApplying(ActionSettingsCondition actionCondition, Product product){
        if(Objects.isNull(product)) {
            return false;
        }

        for(ActionSettingsProduct actionProduct:actionCondition.getProducts()){
            if(actionProduct.getProduct().equals(product)){
                return true;
            }
        }
        return false;
    }

    BigDecimal calculateDiscountSum(Purchase purchase, ActionSettings setting){
        BigDecimal reduce = purchase.getDiscounts().stream().filter(x->x.getActionSettings().equals(setting))
                .filter(x->x.getSum()!=null)
                .map(x->x.getSum()).reduce(BigDecimal.ZERO, BigDecimal::add);
        return reduce;
    }

    BigDecimal calculateCouponsSum(Purchase purchase, ActionSettings setting){
        BigDecimal reduce = purchase.getCoupons().stream().filter(x->x.getActionSettings().equals(setting))
                .filter(x->x.getCoupon()!=null && x.getCoupon().getSum()!=null)
                .map(x->x.getCoupon().getSum()).reduce(BigDecimal.ZERO, BigDecimal::add);
        return reduce;
    }

    boolean isPaymentApplying(ActionSettings actionSettings, Payment payment){
        if (actionSettings.getPayments().isEmpty() ){
            return true;
        }

        if(payment==null) {
            return false;
        }

        Optional<ActionSettingsPayment> optional = actionSettings.getPayments().stream()
                .filter(x->x.getPayment().equals(payment)).findAny();

        return optional.isPresent();
    }

    void applyPurchaseDiscount(Purchase purchase, PurchaseProduct purchaseProduct, BigDecimal discount, ActionSettings setting){
        DiscountPayoutType discountPayoutType = setting.getDiscountPayoutType();
        BigDecimal discountFactor = setting.getDiscountFactor();
        BigDecimal minPrice = BigDecimal.ZERO;

        BigDecimal couponsSum = calculateCouponsSum(purchase, setting);
        if(couponsSum.compareTo(BigDecimal.ZERO)>0){
            BigDecimal discountLimit = couponsSum.subtract(calculateDiscountSum(purchase,setting));
            if(discount.compareTo(discountLimit)>0) discount=discountLimit;
        }

        if(discountFactor.compareTo(new BigDecimal(0))>0){
            discount = discount.multiply(discountFactor);
        }

        if(discountPayoutType== DiscountPayoutType.MANY){
            BigDecimal price = purchaseProduct.getPriceBase().subtract(discount).max(minPrice);
            purchaseProduct.setPrice(price);
            purchaseProduct.setSum(price.multiply(purchaseProduct.getCount()));
            purchase.getDiscounts().add(new PurchaseDiscount(setting, purchaseProduct, discountPayoutType, discount));
        }
        else if (discountPayoutType==DiscountPayoutType.BONUSES){
            purchaseProduct.setBonus(purchaseProduct.getBonus().add(discount));
            purchase.getDiscounts().add(new PurchaseDiscount(setting, purchaseProduct, discountPayoutType, discount));
        }
    }
}
