package com.cubex.promocube.core.processing;

import com.cubex.promocube.core.database.entity.actionsettings.ActionSettings;
import com.cubex.promocube.core.database.entity.product.Product;
import com.cubex.promocube.core.domain.service.PriceListValueService;
import com.cubex.promocube.core.domain.service.ProductUnionValueService;
import com.cubex.promocube.core.processing.actionsettings.ActionSettingsSetPriceValues;
import com.cubex.promocube.core.processing.actionsettings.ActionSettingsSetProducts;
import com.cubex.promocube.core.processing.actionsettings.AlgorithmHandler;
import com.cubex.promocube.core.processing.actionsettings.AlgorithmRequest;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ActionSettingProcessing {
    private PriceListValueService priceListValueService;
    private ProductUnionValueService productUnionValueService;

    public ActionSettingProcessing(PriceListValueService priceListValueService, ProductUnionValueService productUnionValueService) {
        this.priceListValueService = priceListValueService;
        this.productUnionValueService = productUnionValueService;
    }

    private AlgorithmHandler getHandlersChain(){
        AlgorithmHandler h1 = new ActionSettingsSetProducts();
        AlgorithmHandler h2 = new ActionSettingsSetPriceValues();

        h1.setNext(h2);

        return h1;
    }

    public ActionSettings execute(ActionSettings actionSettings, List<Product> products, Date currentDate){
        AlgorithmRequest request = new AlgorithmRequest();
        request.setProducts(products);
        request.setActionSettings(actionSettings);
        request.setProductUnionValues(productUnionValueService.getValues(currentDate));

        List<Long> productsId = request.getProducts().stream().map(x -> x.getId()).collect(Collectors.toList());
        request.setPriceListValues(priceListValueService.getPriceListValues(currentDate,productsId));

        AlgorithmHandler algorithmHandler = getHandlersChain();
        algorithmHandler.handle(request);

        return actionSettings;
    }
}
