package com.cubex.promocube.core.processing.actionsettings;

import com.cubex.promocube.core.database.entity.actionsettings.ActionSettingsCondition;
import com.cubex.promocube.core.database.entity.actionsettings.ActionSettingsProduct;
import com.cubex.promocube.core.database.entity.product.Product;

import java.util.Objects;

public abstract class AlgorithmPrototype implements AlgorithmHandler {
    private AlgorithmHandler nextHandler;

    @Override
    public void handle(AlgorithmRequest request){
        if(canHandleRequest(request)){
            execute(request);
        }

        if(nextHandler!=null){
            nextHandler.handle(request);
        }
    }

    @Override
    public AlgorithmHandler setNext(AlgorithmHandler next) {
        nextHandler = next;
        return nextHandler;
    }

    @Override
    public boolean canHandleRequest(AlgorithmRequest request) {
        return true;
    }

    abstract void execute(AlgorithmRequest request);

}
