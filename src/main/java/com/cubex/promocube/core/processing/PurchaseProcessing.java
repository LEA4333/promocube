package com.cubex.promocube.core.processing;

import com.cubex.promocube.core.database.entity.actionsettings.ActionSettings;
import com.cubex.promocube.core.database.entity.actionsettings.ActionSettingsCouponPromo;
import com.cubex.promocube.core.database.entity.coupon.Coupon;
import com.cubex.promocube.core.database.entity.coupon.CouponStatus;
import com.cubex.promocube.core.database.entity.intersection.ActionIntersection;
import com.cubex.promocube.core.database.entity.product.Product;
import com.cubex.promocube.core.database.entity.purchase.Purchase;
import com.cubex.promocube.core.database.entity.purchase.PurchaseCoupon;
import com.cubex.promocube.core.database.entity.purchase.PurchaseCouponPromo;
import com.cubex.promocube.core.database.entity.purchase.PurchaseProduct;
import com.cubex.promocube.core.domain.service.ActionIntersectionService;
import com.cubex.promocube.core.domain.service.ActionSettingsService;
import com.cubex.promocube.core.processing.purchase.*;
import com.cubex.promocube.core.exceptions.CouponNotActiveException;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class PurchaseProcessing {
    private ActionSettingsService actionSettingsService;
    private ActionIntersectionService actionIntersectionService;

    public PurchaseProcessing(ActionSettingsService actionSettingsService, ActionIntersectionService actionIntersectionService) {
        this.actionSettingsService = actionSettingsService;
        this.actionIntersectionService = actionIntersectionService;
    }

    private AlgorithmHandler getHandlersChain(){
        AlgorithmHandler h1 = new DiscountPercentSummaAlgorithm();
        AlgorithmHandler h2 = new DiscountForPositionSetAlgorithm();
        AlgorithmHandler h3 = new LoyaltyProgramAlgorithm();

        h1.setNext(h2).setNext(h3);

        return h1;
    }

    private void runAlgorithmHandlerChain(Purchase purchase, List<ActionSettings> settings, List<ActionIntersection> actionIntersections, AlgorithmHandler handler){
        for (ActionSettings actionSettings : settings){
            AlgorithmRequest request = new AlgorithmRequest(purchase);
            request.setActionSettings(actionSettings);
            request.setActionIntersections(actionIntersections.stream()
                    .filter(x-> x.isApplying(actionSettings.getAction()))
                    .collect(Collectors.toList()));

            handler.handle(request);
        }
    }

    public void execute(Date currentDate, Purchase purchase){
        checkBeforProcessing(purchase);

        List<ActionSettings> actionSettingsList = actionSettingsService.findAllProcessed(currentDate);
        List<ActionIntersection> actionIntersections = actionIntersectionService.findAll();

        List<Product> productList = purchase.getProducts().stream().map(x->x.getProduct()).collect(Collectors.toList());
        actionSettingsService.processing(actionSettingsList, productList);

        AlgorithmHandler algorithmHandler = getHandlersChain();
        runAlgorithmHandlerChain(purchase, actionSettingsList, actionIntersections, algorithmHandler);

        checkAfterProcessing(purchase);
    }

    public void checkBeforProcessing(Purchase purchase) {
        BigDecimal bonuses = BigDecimal.valueOf(0);
        for (PurchaseProduct purchaseProduct  : purchase.getProducts()) {
            purchaseProduct.setBonus(bonuses);
            purchaseProduct.setPrice(purchaseProduct.getPriceBase());
        }

        for (PurchaseCoupon purchaseCoupon : purchase.getCoupons()) {
            purchaseCoupon.setApplying(false);
            Coupon coupon = purchaseCoupon.getCoupon();
            if(!coupon.getCouponStatus().isActive()){
                throw new CouponNotActiveException(coupon);
            }
        }
    }

    private void checkAfterProcessing(Purchase purchase) {
        checkCouponPromoForPurchase(purchase);
    }

    private void checkCouponPromoForPurchase(Purchase purchase){
        List<ActionSettings> actionSettingsList = purchase.getDiscounts().stream()
                .map(x->x.getActionSettings())
                .filter(x->x!=null && !x.getCouponPromos().isEmpty()).collect(Collectors.toList());

        for (ActionSettings actionSettings: actionSettingsList){
            for (ActionSettingsCouponPromo settingsCouponPromo: actionSettings.getCouponPromos()){
                Coupon coupon = new Coupon();
                coupon.setCouponStatus(CouponStatus.ACTIVATED);
                coupon.setCouponSettings(settingsCouponPromo.getCouponSettings());
                coupon.setSum(settingsCouponPromo.getSum());

                PurchaseCouponPromo purchaseCouponPromo =  new PurchaseCouponPromo();
                purchaseCouponPromo.setCoupon(coupon);
                purchaseCouponPromo.setAction(actionSettings.getAction());
                purchaseCouponPromo.setActionSettings(actionSettings);

                purchase.getCouponPromos().add(purchaseCouponPromo);
            }
        }
    }
}
