package com.cubex.promocube.core.controller.entity;

import com.cubex.promocube.core.domain.facade.ActionIntersectionFacade;
import com.cubex.promocube.core.dto.intersection.ActionIntersectionDto;
import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api
@RestController
@RequestMapping("/api/actionIntersections")
public class ActionIntersectionController {
    private final ActionIntersectionFacade facade;

    public ActionIntersectionController(ActionIntersectionFacade facade) {
        this.facade = facade;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody @Valid ActionIntersectionDto dto){
        facade.create(dto);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ActionIntersectionDto getById(@PathVariable Long id){
        return facade.findById(id).get();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void delete(@PathVariable Long id){
        facade.delete(id);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<ActionIntersectionDto> selectAll(){
        return facade.selectAll();
    }
}
