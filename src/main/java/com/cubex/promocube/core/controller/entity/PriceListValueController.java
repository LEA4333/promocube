package com.cubex.promocube.core.controller.entity;

import com.cubex.promocube.core.domain.facade.PriceListValueFacade;
import com.cubex.promocube.core.dto.pricing.PriceListValueDto;
import com.cubex.promocube.core.dto.pricing.PriceListValuesRequestDto;
import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api
@RestController
@RequestMapping("/api/priceListValue")
public class PriceListValueController {
    private PriceListValueFacade facade;

    public PriceListValueController(PriceListValueFacade facade) {
        this.facade = facade;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody @Valid PriceListValueDto dto){
        facade.create(dto);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public PriceListValueDto getById(@PathVariable Long id){
        return facade.findById(id).get();
    }

    @PostMapping("/current")
    @ResponseStatus(HttpStatus.OK)
    public List<PriceListValueDto> getPriceListValues(@RequestBody @Valid PriceListValuesRequestDto dto){
        return facade.getPriceListValues(dto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void delete(@PathVariable Long id){
        facade.delete(id);
    }
}
