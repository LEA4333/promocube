package com.cubex.promocube.core.controller.entity;

import com.cubex.promocube.core.domain.facade.ActionFacade;
import com.cubex.promocube.core.domain.facade.DepartmentFacade;
import com.cubex.promocube.core.dto.action.ActionDto;
import com.cubex.promocube.core.dto.department.DepartmentDto;
import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api
@RestController
@RequestMapping("/api/department")
public class DepartmentController {
    private DepartmentFacade facade;

    public DepartmentController(DepartmentFacade facade) {
        this.facade = facade;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody @Valid DepartmentDto dto){
        facade.create(dto);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public DepartmentDto getById(@PathVariable Long id){
        return facade.findById(id).get();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void delete(@PathVariable Long id){
        facade.delete(id);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<DepartmentDto> selectAll(){
        return facade.findAll();
    }
}
