package com.cubex.promocube.core.controller.entity;

import com.cubex.promocube.core.domain.facade.ActionSettingsFacade;
import com.cubex.promocube.core.dto.actionsettigs.ActionSettingsDto;
import com.cubex.promocube.core.dto.purchase.RequestActionSettingsProcessedDto;
import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api
@RestController
@RequestMapping("/api/actionSettings")
public class ActionSettingsController {
    private final ActionSettingsFacade facade;

    public ActionSettingsController(ActionSettingsFacade facade) {
        this.facade = facade;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody @Valid ActionSettingsDto dto){
        facade.create(dto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void delete(@PathVariable Long id){
        facade.delete(id);
    }

    @PostMapping("/current")
    @ResponseStatus(HttpStatus.OK)
    public List<ActionSettingsDto> selectProcessed(@RequestBody @Valid RequestActionSettingsProcessedDto dto){
        return facade.findAllProcessed(dto.getDate());
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<ActionSettingsDto> selectAll(){
        return facade.findAll();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ActionSettingsDto getById(Long id){
        return facade.findById(id).get();
    }
}
