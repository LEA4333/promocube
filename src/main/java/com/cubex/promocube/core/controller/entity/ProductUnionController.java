package com.cubex.promocube.core.controller.entity;

import com.cubex.promocube.core.domain.facade.ProductUnionFacade;
import com.cubex.promocube.core.dto.productUnion.ProductUnionDto;
import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api
@RestController
@RequestMapping("/api/productUnion")
public class ProductUnionController {
    private ProductUnionFacade facade;

    public ProductUnionController(ProductUnionFacade facade) {
        this.facade = facade;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody @Valid ProductUnionDto dto){
        facade.create(dto);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ProductUnionDto getById(@PathVariable Long id){
        return facade.findById(id).get();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void delete(@PathVariable Long id){
        facade.delete(id);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<ProductUnionDto> selectAll(){
        return facade.findAll();
    }
}
