package com.cubex.promocube.core.controller.entity;

import com.cubex.promocube.core.domain.facade.ActionFacade;
import com.cubex.promocube.core.dto.action.ActionDto;
import com.cubex.promocube.core.dto.payment.PaymentDto;
import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api
@RestController
@RequestMapping("/api/action")
public class ActionController {
    private ActionFacade facade;

    public ActionController(ActionFacade facade) {
        this.facade = facade;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody @Valid ActionDto dto){
        facade.create(dto);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ActionDto getById(@PathVariable Long id){
        return facade.findById(id).get();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void delete(@PathVariable Long id){
        facade.delete(id);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<ActionDto> selectAll(){
        return facade.findAll();
    }
}
