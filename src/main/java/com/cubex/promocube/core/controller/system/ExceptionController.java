package com.cubex.promocube.core.controller.system;

import com.cubex.promocube.core.dto.system.ApiErrorDto;
import org.modelmapper.MappingException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.NoSuchElementException;

@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler(value = NoSuchElementException.class)
    public ResponseEntity<ApiErrorDto> NoSuchElementException(NoSuchElementException exception, HttpServletRequest request) {
        ApiErrorDto errorDto = new ApiErrorDto(HttpStatus.NOT_FOUND,exception);
        errorDto.setPath(request.getRequestURI());

        exception.printStackTrace();

        return new ResponseEntity<>(errorDto, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = MappingException.class)
    public ResponseEntity<ApiErrorDto> handleMappingException(MappingException exception, HttpServletRequest request) {
        ApiErrorDto errorDto = new ApiErrorDto(HttpStatus.INTERNAL_SERVER_ERROR,exception);
        errorDto.setPath(request.getRequestURI());

        if(exception.getCause()!=null){
            Throwable cause = exception.getCause();
            errorDto.setMessage(cause.getMessage());
        }

        exception.printStackTrace();

        return new ResponseEntity<>(errorDto, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<ApiErrorDto> handle(RuntimeException exception, HttpServletRequest request) {
        ApiErrorDto errorDto = new ApiErrorDto(HttpStatus.INTERNAL_SERVER_ERROR,exception);
        errorDto.setPath(request.getRequestURI());

        exception.printStackTrace();

        return new ResponseEntity<>(errorDto, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler
    public ResponseEntity<ApiErrorDto> handle(MethodArgumentNotValidException exception, HttpServletRequest request){
        ApiErrorDto errorDto = new ApiErrorDto(HttpStatus.BAD_REQUEST);
        errorDto.setMessage(exception.getMessage());
        errorDto.setPath(request.getRequestURI());
        return new ResponseEntity<>(errorDto, HttpStatus.BAD_REQUEST);
    }
}
