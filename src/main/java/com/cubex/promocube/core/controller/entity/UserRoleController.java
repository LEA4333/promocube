package com.cubex.promocube.core.controller.entity;

import com.cubex.promocube.core.domain.facade.UserRoleFacade;
import com.cubex.promocube.core.dto.user.UserRoleDto;
import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api
@RestController
@RequestMapping("/api/userRole")
public class UserRoleController {
    private UserRoleFacade facade;

    public UserRoleController(UserRoleFacade facade) {
        this.facade = facade;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody @Valid UserRoleDto dto){
        facade.create(dto);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public UserRoleDto getById(@PathVariable Long id){
        return facade.findById(id).get();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void delete(@PathVariable Long id){
        facade.delete(id);
    }
}
