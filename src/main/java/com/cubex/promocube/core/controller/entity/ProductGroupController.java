package com.cubex.promocube.core.controller.entity;

import com.cubex.promocube.core.domain.facade.ProductGroupFacade;
import com.cubex.promocube.core.dto.product.ProductGroupDto;
import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api
@RestController
@RequestMapping("/api/productGroup")
public class ProductGroupController {
    private ProductGroupFacade facade;

    public ProductGroupController(ProductGroupFacade facade) {
        this.facade = facade;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody @Valid ProductGroupDto dto){
        facade.create(dto);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ProductGroupDto getById(@PathVariable Long id){
        return facade.findById(id).get();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void delete(@PathVariable Long id){
        facade.delete(id);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<ProductGroupDto> selectAll(){
        return facade.findAll();
    }
}
