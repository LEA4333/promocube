package com.cubex.promocube.core.controller.system;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ApiIgnore
@RestController
@RequestMapping("/")
public class IndexController {
    @RequestMapping(value={"/api","/"})
    void handleFoo(HttpServletResponse response) throws IOException {
        response.sendRedirect("/swagger-ui.html#/");
    }
}
