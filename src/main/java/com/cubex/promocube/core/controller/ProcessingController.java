package com.cubex.promocube.core.controller;

import com.cubex.promocube.core.database.entity.actionsettings.ActionSettings;
import com.cubex.promocube.core.domain.facade.ActionSettingsFacade;
import com.cubex.promocube.core.domain.facade.PurchaseFacade;
import com.cubex.promocube.core.dto.actionsettigs.ActionSettingsDto;
import com.cubex.promocube.core.dto.purchase.PurchaseDto;
import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api
@RestController
@RequestMapping("/processing")
public class ProcessingController {
    private PurchaseFacade purchaseFacade;
    private ActionSettingsFacade actionSettingsFacade;

    public ProcessingController(PurchaseFacade purchaseFacade, ActionSettingsFacade actionSettingsFacade) {
        this.purchaseFacade = purchaseFacade;
        this.actionSettingsFacade = actionSettingsFacade;
    }

    @PostMapping("/purchase")
    @ResponseStatus(HttpStatus.CREATED)
    public PurchaseDto processing(@RequestBody @Valid PurchaseDto dto){
        return purchaseFacade.processing(dto);
    }

    @GetMapping("/actionSettings/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ActionSettingsDto actionSettingsGetById(@PathVariable Long id){
        return actionSettingsFacade.processing(id).get();
    }
}
