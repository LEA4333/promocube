package com.cubex.promocube.core.exceptions;

import com.cubex.promocube.core.database.entity.coupon.Coupon;

public class CouponApplayingErrorException extends RuntimeException {
    public CouponApplayingErrorException(Coupon coupon) {
        super("Coupon '"+coupon.getName()+"' not applaying becouse of error status");
    }
}
