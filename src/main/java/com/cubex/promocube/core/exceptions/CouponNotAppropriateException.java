package com.cubex.promocube.core.exceptions;

import com.cubex.promocube.core.database.entity.coupon.Coupon;

public class CouponNotAppropriateException extends RuntimeException {
    public CouponNotAppropriateException(Coupon coupon) {
        super("Coupon '"+coupon.getName()+"' not appropriate for any action");
    }
}
