package com.cubex.promocube.core.exceptions;

public class CouponNotFoundException extends RuntimeException {
    public CouponNotFoundException(String couponeName) {
        super("Coupon '"+couponeName+"' not found");
    }
}
