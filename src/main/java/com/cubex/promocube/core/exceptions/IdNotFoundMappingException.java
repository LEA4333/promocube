package com.cubex.promocube.core.exceptions;

import org.modelmapper.MappingException;
import org.modelmapper.spi.ErrorMessage;

import java.util.Collections;
import java.util.Formatter;
import java.util.List;

public class IdNotFoundMappingException extends MappingException {
    private static List<ErrorMessage> getMessage(Long id, String fieldName){
        Formatter f = new Formatter();
        String message = f.format("Id '%s' not found for field '%s'", id, fieldName).toString();
        return Collections.singletonList(new ErrorMessage(message));
    }

    public IdNotFoundMappingException(Long id, String fieldName) {
        super(getMessage(id, fieldName));
    }
}
