package com.cubex.promocube.core.domain.service;

import com.cubex.promocube.core.database.entity.purchase.Purchase;
import com.cubex.promocube.core.processing.CouponProcessing;
import com.cubex.promocube.core.processing.PurchaseProcessing;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
public class ProcessingService {
    private PurchaseProcessing processing;
    private PurchaseService purchaseService;
    private CouponProcessing couponProcessing;

    public ProcessingService(PurchaseProcessing processing, PurchaseService purchaseService, CouponProcessing couponProcessing) {
        this.processing = processing;
        this.purchaseService = purchaseService;
        this.couponProcessing = couponProcessing;
    }

    @Transactional
    public Purchase processing(Purchase purchase) {
        Date date = new Date();
        processing.execute(date,purchase);
        purchase = purchaseService.create(purchase);
        couponProcessing.execute(purchase);
        return purchase;
    }
}
