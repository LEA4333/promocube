package com.cubex.promocube.core.domain.facade;

import com.cubex.promocube.core.database.entity.intersection.ActionIntersection;
import com.cubex.promocube.core.domain.service.ActionIntersectionService;
import com.cubex.promocube.core.dto.intersection.ActionIntersectionDto;
import com.cubex.promocube.core.mapper.ActionIntersectionsMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class ActionIntersectionFacade {
    private final ActionIntersectionService service;
    private final ActionIntersectionsMapper mapper;

    public ActionIntersectionFacade(ActionIntersectionService service, ActionIntersectionsMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    public void create(ActionIntersectionDto dto) {
        Optional<ActionIntersectionDto> optional = Optional.of(dto);
        service.create(optional.map(mapper::toEntity).get());
    }

    public Optional<ActionIntersectionDto> findById(Long id){
        Optional<ActionIntersection> optional = service.findById(id);
        return optional.map(mapper::toDto);
    }

    public void delete(Long id) {
        service.delete(id);
    }

    public List<ActionIntersectionDto> selectAll(){
        return service.findAll().stream().map(mapper::toDto).collect(Collectors.toList());
    }

}
