package com.cubex.promocube.core.domain.service;

import com.cubex.promocube.core.database.entity.action.Action;
import com.cubex.promocube.core.database.entity.purchase.Purchase;
import com.cubex.promocube.core.database.entity.purchase.PurchaseProduct;
import com.cubex.promocube.core.database.repository.CouponRepo;
import com.cubex.promocube.core.database.repository.PurchaseProductRepo;
import com.cubex.promocube.core.database.repository.PurchaseRepo;
import com.cubex.promocube.core.processing.PurchaseProcessing;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
public class PurchaseProductService {
    private PurchaseProductRepo repo;

    public PurchaseProductService(PurchaseProductRepo repo) {
        this.repo = repo;
    }

    public Optional<PurchaseProduct> findById(Long id){
        return  repo.findById(id);
    }

    public Long getId(PurchaseProduct item) {
        return Objects.isNull(item) ? null : item.getId();
    }
}
