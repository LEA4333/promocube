package com.cubex.promocube.core.domain.service;

import com.cubex.promocube.core.database.entity.coupon.Coupon;
import com.cubex.promocube.core.database.entity.coupon.CouponStatus;
import com.cubex.promocube.core.database.repository.CouponRepo;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CouponService {
    private CouponRepo repo;

    public CouponService(CouponRepo repo) {
        this.repo = repo;
    }

    public void create(Coupon coupon) {
        repo.save(coupon);
    }

    public Optional<Coupon> findById(Long id){
        return repo.findById(id);
    }

    public Optional<Coupon> findByName(String name) {
        return repo.findByName(name);
    }

    public synchronized boolean changeCouponStatus(Coupon coupon,CouponStatus couponStatusFrom, CouponStatus couponStatusTarget){
        Optional<Coupon> optional = repo.findByIdAndCouponStatus(coupon.getId(),couponStatusFrom);
        if(optional.isEmpty()) return false;

        Coupon target = optional.get();
        target.setCouponStatus(couponStatusTarget);
        repo.save(target);

        return true;
    }

    public void delete(Long id) {
        repo.deleteById(id);
    }
}
