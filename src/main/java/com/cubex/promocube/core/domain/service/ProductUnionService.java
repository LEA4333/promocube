package com.cubex.promocube.core.domain.service;

import com.cubex.promocube.core.database.entity.product.ProductUnion;
import com.cubex.promocube.core.database.repository.ProductUnionRepo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class ProductUnionService {
    private ProductUnionRepo repo;

    public ProductUnionService(ProductUnionRepo productRepo) {
        this.repo = productRepo;
    }

    public void create(ProductUnion productUnion) {
        repo.save(productUnion);
    }

    public Optional<ProductUnion> findById(Long id){
        return repo.findById(id);
    }

    public Long getId(ProductUnion productUnion) {
        return Objects.isNull(productUnion) ? null : productUnion.getId();
    }

    public void delete(Long id) {
        repo.deleteById(id);
    }

    public List<ProductUnion> findAll() {
        return (List<ProductUnion>) repo.findAll();
    }
}
