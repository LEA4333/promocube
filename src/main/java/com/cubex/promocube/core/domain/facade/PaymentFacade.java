package com.cubex.promocube.core.domain.facade;

import com.cubex.promocube.core.database.entity.actionsettings.ActionSettings;
import com.cubex.promocube.core.database.entity.payment.Payment;
import com.cubex.promocube.core.domain.service.PaymentService;
import com.cubex.promocube.core.dto.payment.PaymentDto;
import com.cubex.promocube.core.mapper.PaymentMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
public class PaymentFacade {
    private PaymentService service;
    private PaymentMapper mapper;

    public PaymentFacade(PaymentService service, PaymentMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    public void create(PaymentDto dto) {
        Optional<PaymentDto> optional = Optional.of(dto);
        service.create(optional.map(mapper::toEntity).get());
    }

    public Optional<PaymentDto> findById(Long id){
        Optional<Payment> optional = service.findById(id);
        return optional.map(mapper::toDto);
    }

    public void delete(Long id) {
        service.delete(id);
    }

    public List<PaymentDto> findAll() {
        return StreamSupport.stream(service.findAll().spliterator(), false)
                .map(mapper::toDto)
                .collect(Collectors.toList());
    }
}
