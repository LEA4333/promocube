package com.cubex.promocube.core.domain.service;

import com.cubex.promocube.core.database.entity.department.Department;
import com.cubex.promocube.core.database.repository.DepartmentRepo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class DepartmentService {
    private DepartmentRepo repo;

    public DepartmentService(DepartmentRepo repo) {
        this.repo = repo;
    }

    public void create(Department item) {
        repo.save(item);
    }

    public Optional<Department> findById(Long id){
        return repo.findById(id);
    }

    public void delete(Long id) {
        repo.deleteById(id);
    }

    public List<Department> findAll() {
        return (List<Department>) repo.findAll();
    }

    public Long getId(Department item) {
        return Objects.isNull(item) ? null : item.getId();
    }
}
