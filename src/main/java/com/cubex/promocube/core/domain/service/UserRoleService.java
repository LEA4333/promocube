package com.cubex.promocube.core.domain.service;

import com.cubex.promocube.core.database.entity.user.UserRole;
import com.cubex.promocube.core.database.repository.UserRoleRepo;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
public class UserRoleService {
    private UserRoleRepo repo;

    public UserRoleService(UserRoleRepo repo) {
        this.repo = repo;
    }

    public void create(UserRole item) {
        repo.save(item);
    }

    public Optional<UserRole> findById(Long id){
        return  repo.findById(id);
    }

    public Long getId(UserRole item) {
        return Objects.isNull(item) ? null : item.getId();
    }

    public void delete(Long id) {
        repo.deleteById(id);
    }
}
