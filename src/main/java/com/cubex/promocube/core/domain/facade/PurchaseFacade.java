package com.cubex.promocube.core.domain.facade;

import com.cubex.promocube.core.database.entity.purchase.Purchase;
import com.cubex.promocube.core.domain.service.ProcessingService;
import com.cubex.promocube.core.domain.service.PurchaseService;
import com.cubex.promocube.core.dto.purchase.PurchaseDto;
import com.cubex.promocube.core.mapper.PurchaseMapper;
import org.springframework.stereotype.Component;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Optional;

@Component
public class PurchaseFacade {
    private PurchaseService service;
    private PurchaseMapper mapper;
    private ProcessingService processingService;

    public PurchaseFacade(PurchaseService service, PurchaseMapper mapper, ProcessingService processingService) {
        this.service = service;
        this.mapper = mapper;
        this.processingService = processingService;
    }

    public void create(PurchaseDto dto) {
        Optional<PurchaseDto> optional = Optional.of(dto);
        service.create(optional.map(mapper::toEntity).get());
    }

    public PurchaseDto processing(PurchaseDto dto) {
        Optional<PurchaseDto> optional = Optional.of(dto);
        Purchase purchase = optional.map(mapper::toEntity).get();

        Purchase target = processingService.processing(purchase);

        Optional<Purchase> optionalForResult = Optional.of(target);
        return optionalForResult.map(mapper::toDto).get();
    }

    public Optional<PurchaseDto> findById(Long id){
        Optional<Purchase> optional = service.findById(id);
        return optional.map(mapper::toDto);
    }

    public void delete(Long id) {
        service.delete(id);
    }
}
