package com.cubex.promocube.core.domain.facade;

import com.cubex.promocube.core.database.entity.product.Product;
import com.cubex.promocube.core.domain.service.ProductService;
import com.cubex.promocube.core.dto.product.ProductDto;
import com.cubex.promocube.core.mapper.ProductMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
public class ProductFacade {
    private ProductService service;
    private ProductMapper mapper;

    public ProductFacade(ProductService service, ProductMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }


    public void create(ProductDto dto) {
        Optional<ProductDto> optional = Optional.of(dto);
        service.create(optional.map(mapper::toEntity).get());
    }

    public Optional<ProductDto> findById(Long id){
        Optional<Product> optional = service.findById(id);
        return optional.map(mapper::toDto);
    }

    public void delete(Long id) {
        service.delete(id);
    }

    public List<ProductDto> findAll() {
        return StreamSupport.stream(service.findAll().spliterator(), false)
                .map(mapper::toDto)
                .collect(Collectors.toList());
    }
}
