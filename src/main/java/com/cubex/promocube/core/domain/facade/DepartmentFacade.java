package com.cubex.promocube.core.domain.facade;

import com.cubex.promocube.core.database.entity.department.Department;
import com.cubex.promocube.core.domain.service.DepartmentService;
import com.cubex.promocube.core.dto.department.DepartmentDto;
import com.cubex.promocube.core.mapper.DepartmentMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
public class DepartmentFacade {
    private DepartmentService service;
    private DepartmentMapper mapper;

    public DepartmentFacade(DepartmentService service, DepartmentMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    public void create(DepartmentDto dto) {
        Optional<DepartmentDto> optional = Optional.of(dto);
        service.create(optional.map(mapper::toEntity).get());
    }

    public Optional<DepartmentDto> findById(Long id){
        Optional<Department> optional = service.findById(id);
        return optional.map(mapper::toDto);
    }

    public void delete(Long id) {
        service.delete(id);
    }

    public List<DepartmentDto> findAll() {
        return StreamSupport.stream(service.findAll().spliterator(), false)
                .map(mapper::toDto)
                .collect(Collectors.toList());
    }
}
