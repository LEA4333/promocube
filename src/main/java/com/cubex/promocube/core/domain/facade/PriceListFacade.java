package com.cubex.promocube.core.domain.facade;

import com.cubex.promocube.core.database.entity.pricing.PriceList;
import com.cubex.promocube.core.domain.service.PriceListService;
import com.cubex.promocube.core.dto.pricing.PriceListDto;
import com.cubex.promocube.core.mapper.PriceListMapper;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class PriceListFacade {
    private PriceListService service;
    private PriceListMapper mapper;

    public PriceListFacade(PriceListService service, PriceListMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    public void create(PriceListDto dto) {
        Optional<PriceListDto> optional = Optional.of(dto);
        service.create(optional.map(mapper::toEntity).get());
    }

    public Optional<PriceListDto> findById(Long id){
        Optional<PriceList> optional = service.findById(id);
        return optional.map(mapper::toDto);
    }

    public void delete(Long id) {
        service.delete(id);
    }
}
