package com.cubex.promocube.core.domain.service;

import com.cubex.promocube.core.database.entity.coupon.CouponSettings;
import com.cubex.promocube.core.database.repository.CouponSettingsRepo;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
public class CouponSettingsService {
    private CouponSettingsRepo repo;

    public CouponSettingsService(CouponSettingsRepo repo) {
        this.repo = repo;
    }

    public void create(CouponSettings couponSettings) {
        repo.save(couponSettings);
    }

    public Optional<CouponSettings> findById(Long id){
        return repo.findById(id);
    }

    public void delete(Long id) {
        repo.deleteById(id);
    }

    public Long getId(CouponSettings item) {
        return Objects.isNull(item) ? null : item.getId();
    }
}
