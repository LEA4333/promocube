package com.cubex.promocube.core.domain.facade;

import com.cubex.promocube.core.database.entity.coupon.CouponSettings;
import com.cubex.promocube.core.domain.service.CouponSettingsService;
import com.cubex.promocube.core.dto.coupon.CouponSettingsDto;
import com.cubex.promocube.core.mapper.CouponSettingsMapper;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class CouponSettingsFacade {
    private CouponSettingsService service;
    private CouponSettingsMapper mapper;

    public CouponSettingsFacade(CouponSettingsService service, CouponSettingsMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    public void create(CouponSettingsDto dto) {
        Optional<CouponSettingsDto> optional = Optional.of(dto);
        service.create(optional.map(mapper::toEntity).get());
    }

    public Optional<CouponSettingsDto> findById(Long id){
        Optional<CouponSettings> optional = service.findById(id);
        return optional.map(mapper::toDto);
    }

    public void delete(Long id) {
        service.delete(id);
    }
}
