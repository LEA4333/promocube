package com.cubex.promocube.core.domain.facade;

import com.cubex.promocube.core.database.entity.coupon.Coupon;
import com.cubex.promocube.core.domain.service.CouponService;
import com.cubex.promocube.core.dto.coupon.CouponDto;
import com.cubex.promocube.core.mapper.CouponMapper;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class CouponFacade {
    private CouponService service;
    private CouponMapper mapper;

    public CouponFacade(CouponService service, CouponMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    public void create(CouponDto dto) {
        Optional<CouponDto> optional = Optional.of(dto);
        service.create(optional.map(mapper::toEntity).get());
    }

    public Optional<CouponDto> findById(Long id){
        Optional<Coupon> optional = service.findById(id);
        return optional.map(mapper::toDto);
    }

    public void delete(Long id) {
        service.delete(id);
    }
}
