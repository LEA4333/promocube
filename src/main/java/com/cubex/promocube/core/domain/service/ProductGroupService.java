package com.cubex.promocube.core.domain.service;

import com.cubex.promocube.core.database.entity.product.Product;
import com.cubex.promocube.core.database.entity.product.ProductGroup;
import com.cubex.promocube.core.database.repository.ProductGroupRepo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class ProductGroupService {
    private ProductGroupRepo repo;

    public ProductGroupService(ProductGroupRepo productRepo) {
        this.repo = productRepo;
    }

    public void create(ProductGroup productGroup) {
        repo.save(productGroup);
    }

    public Optional<ProductGroup> findById(Long id){
        return repo.findById(id);
    }

    public Long getId(Product product) {
        return Objects.isNull(product) ? null : product.getId();
    }

    public void delete(Long id) {
        repo.deleteById(id);
    }

    public List<ProductGroup> findAll() {
        return (List<ProductGroup>) repo.findAll();
    }
}
