package com.cubex.promocube.core.domain.facade;

import com.cubex.promocube.core.database.entity.pricing.PriceListValue;
import com.cubex.promocube.core.domain.service.PriceListValueService;
import com.cubex.promocube.core.dto.pricing.PriceListValueDto;
import com.cubex.promocube.core.dto.pricing.PriceListValuesRequestDto;
import com.cubex.promocube.core.mapper.PriceListValueMapper;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class PriceListValueFacade {
    private PriceListValueService service;
    private PriceListValueMapper mapper;

    public PriceListValueFacade(PriceListValueService service, PriceListValueMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    public void create(PriceListValueDto dto) {
        Optional<PriceListValueDto> optional = Optional.of(dto);
        service.create(optional.map(mapper::toEntity).get());
    }

    public Optional<PriceListValueDto> findById(Long id){
        Optional<PriceListValue> optional = service.findById(id);
        return optional.map(mapper::toDto);
    }

    public List<PriceListValueDto> getPriceListValues(PriceListValuesRequestDto dto){
        return service.getPriceListValues(dto.getPeriod(),dto.getProductsId()).stream()
                .map(mapper::toDto)
                .collect(Collectors.toList());
    }

    public void delete(Long id) {
        service.delete(id);
    }
}
