package com.cubex.promocube.core.domain.service;

import com.cubex.promocube.core.database.entity.actionsettings.ActionSettings;
import com.cubex.promocube.core.database.entity.product.Product;
import com.cubex.promocube.core.database.repository.ActionSettingsRepo;
import com.cubex.promocube.core.database.repository.ProductRepo;
import com.cubex.promocube.core.processing.ActionSettingProcessing;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ActionSettingsService {
    private final ActionSettingsRepo repo;
    private final ProductRepo productRepo;
    private final ActionSettingProcessing processing;

    public ActionSettingsService(ActionSettingsRepo repo, ProductRepo productRepo, ActionSettingProcessing processing) {
        this.repo = repo;
        this.productRepo = productRepo;
        this.processing = processing;
    }

    public void create(ActionSettings settings) {
        repo.save(settings);
    }

    public void delete(Long id) {
        repo.deleteById(id);
    }

    public List<ActionSettings> findAllProcessed(Date date){
        return repo.findAllProcessed(date);
    }

    public Optional<ActionSettings> processing(Long id) {
        Optional<ActionSettings> optional = repo.findById(id);

        if(optional.isPresent()){
            ActionSettings actionSettings = optional.get();

            List<Product> products = productRepo.findTop100By();

            processing.execute(actionSettings, products, new Date());
            return Optional.of(actionSettings);
        }

        return Optional.empty();
    }

    public void processing(List<ActionSettings> actionSettingsList, List<Product> productList) {
        for (ActionSettings actionSettings : actionSettingsList) {
            processing.execute(actionSettings, productList, new Date());
        }
    }

    public Long getId(ActionSettings actionSettings) {
        return Objects.isNull(actionSettings) ? null : actionSettings.getId();
    }

    public Optional<ActionSettings> findById(Long id){
        return repo.findById(id);
    }

    public List<ActionSettings> findAll() {
        return (List<ActionSettings>) repo.findAll();
    }
}
