package com.cubex.promocube.core.domain.facade;

import com.cubex.promocube.core.database.entity.product.ProductUnion;
import com.cubex.promocube.core.domain.service.ProductUnionService;
import com.cubex.promocube.core.dto.productUnion.ProductUnionDto;
import com.cubex.promocube.core.mapper.ProductUnionMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
public class ProductUnionFacade {
    private ProductUnionService service;
    private ProductUnionMapper mapper;

    public ProductUnionFacade(ProductUnionService service, ProductUnionMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }


    public void create(ProductUnionDto dto) {
        Optional<ProductUnionDto> optional = Optional.of(dto);
        service.create(optional.map(mapper::toEntity).get());
    }

    public Optional<ProductUnionDto> findById(Long id){
        Optional<ProductUnion> optional = service.findById(id);
        return optional.map(mapper::toDto);
    }

    public void delete(Long id) {
        service.delete(id);
    }

    public List<ProductUnionDto> findAll() {
        return StreamSupport.stream(service.findAll().spliterator(), false)
                .map(mapper::toDto)
                .collect(Collectors.toList());
    }
}
