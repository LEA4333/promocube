package com.cubex.promocube.core.domain.service;

import com.cubex.promocube.core.database.entity.action.Action;
import com.cubex.promocube.core.database.entity.payment.Payment;
import com.cubex.promocube.core.database.repository.ActionRepo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class ActionService {
    private ActionRepo repo;

    public ActionService(ActionRepo repo) {
        this.repo = repo;
    }

    public void create(Action action) {
        repo.save(action);
    }

    public Optional<Action> findById(Long id){
        return repo.findById(id);
    }

    public void delete(Long id) {
        repo.deleteById(id);
    }

    public Long getId(Action action) {
        return Objects.isNull(action) ? null : action.getId();
    }

    public List<Action> findAll() {
        return (List<Action>) repo.findAll();
    }
}
