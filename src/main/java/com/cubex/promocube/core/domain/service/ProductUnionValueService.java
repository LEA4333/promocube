package com.cubex.promocube.core.domain.service;

import com.cubex.promocube.core.database.entity.productUnion.ProductUnionValue;
import com.cubex.promocube.core.database.repository.ProductUnionValueRepo;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ProductUnionValueService {
    private ProductUnionValueRepo repo;

    public ProductUnionValueService(ProductUnionValueRepo repo) {
        this.repo = repo;
    }

    public void create(ProductUnionValue item) {
        repo.save(item);
    }

    public Optional<ProductUnionValue> findById(Long id){
        return  repo.findById(id);
    }

    public List<ProductUnionValue> getValues(Date period){
        return repo.getValues(period);
    }

    public List<ProductUnionValue> getValues(Date period,List<Long> productsId){
        return repo.getValues(period,productsId);
    }

    public void delete(Long id) {
        repo.deleteById(id);
    }

    public List<ProductUnionValue> findAll() {
        return (List<ProductUnionValue>) repo.findAll();
    }
}
