package com.cubex.promocube.core.domain.facade;

import com.cubex.promocube.core.database.entity.product.ProductGroup;
import com.cubex.promocube.core.domain.service.ProductGroupService;
import com.cubex.promocube.core.dto.product.ProductGroupDto;
import com.cubex.promocube.core.mapper.ProductGroupMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
public class ProductGroupFacade {
    private ProductGroupService service;
    private ProductGroupMapper mapper;

    public ProductGroupFacade(ProductGroupService service, ProductGroupMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }


    public void create(ProductGroupDto dto) {
        Optional<ProductGroupDto> optional = Optional.of(dto);
        service.create(optional.map(mapper::toEntity).get());
    }

    public Optional<ProductGroupDto> findById(Long id){
        Optional<ProductGroup> optional = service.findById(id);
        return optional.map(mapper::toDto);
    }

    public void delete(Long id) {
        service.delete(id);
    }

    public List<ProductGroupDto> findAll() {
        return StreamSupport.stream(service.findAll().spliterator(), false)
                .map(mapper::toDto)
                .collect(Collectors.toList());
    }
}
