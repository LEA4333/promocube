package com.cubex.promocube.core.domain.facade;

import com.cubex.promocube.core.database.entity.actionsettings.ActionSettings;
import com.cubex.promocube.core.domain.service.ActionSettingsService;
import com.cubex.promocube.core.dto.actionsettigs.ActionSettingsDto;
import com.cubex.promocube.core.mapper.ActionSettingsMapper;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
public class ActionSettingsFacade {
    private ActionSettingsService service;
    private ActionSettingsMapper mapper;

    public ActionSettingsFacade(ActionSettingsService service, ActionSettingsMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    public void create(ActionSettingsDto dto) {
        Optional<ActionSettingsDto> optional = Optional.of(dto);
        service.create(optional.map(mapper::toEntity).get());
    }

    public void delete(Long id) {
        service.delete(id);
    }

    public List<ActionSettingsDto> findAllProcessed(Date date) {
        return StreamSupport.stream(service.findAllProcessed(date).spliterator(), false)
                .map(mapper::toDto)
                .collect(Collectors.toList());
    }

    public Optional<ActionSettingsDto> findById(Long id){
        Optional<ActionSettings> optional = service.findById(id);
        return optional.map(mapper::toDto);
    }

    public List<ActionSettingsDto> findAll() {
        return StreamSupport.stream(service.findAll().spliterator(), false)
                .map(mapper::toDto)
                .collect(Collectors.toList());
    }

    public Optional<ActionSettingsDto> processing(Long id) {
        Optional<ActionSettings> optional = service.processing(id);
        return optional.map(mapper::toDto);
    }
}
