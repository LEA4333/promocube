package com.cubex.promocube.core.domain.facade;

import com.cubex.promocube.core.database.entity.productUnion.ProductUnionValue;
import com.cubex.promocube.core.domain.service.ProductUnionValueService;
import com.cubex.promocube.core.dto.list.ProductListRequestDto;
import com.cubex.promocube.core.dto.productUnion.ProductUnionValueDto;
import com.cubex.promocube.core.mapper.ProductUnionValueMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
public class ProductUnionValueFacade {
    private ProductUnionValueService service;
    private ProductUnionValueMapper mapper;

    public ProductUnionValueFacade(ProductUnionValueService service, ProductUnionValueMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    public void create(ProductUnionValueDto dto) {
        Optional<ProductUnionValueDto> optional = Optional.of(dto);
        service.create(optional.map(mapper::toEntity).get());
    }

    public Optional<ProductUnionValueDto> findById(Long id){
        Optional<ProductUnionValue> optional = service.findById(id);
        return optional.map(mapper::toDto);
    }

    public List<ProductUnionValueDto> getValues(ProductListRequestDto dto){
        return service.getValues(dto.getPeriod(),dto.getProductsId()).stream()
                .map(mapper::toDto)
                .collect(Collectors.toList());
    }

    public void delete(Long id) {
        service.delete(id);
    }

    public List<ProductUnionValueDto> selectAll() {
        return StreamSupport.stream(service.findAll().spliterator(), false)
                .map(mapper::toDto)
                .collect(Collectors.toList());
    }
}
